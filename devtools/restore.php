<?php

// define( 'WP_DEBUG', true );


define( 'DB_NAME', 'wcsa2meli' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'wcsa2meli' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'wcsa2meli' );

/** Nome do host do MySQL */
define( 'DB_HOST', '127.0.0.1' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

$filePath = 'dumps/dump.sql';

//Deleta a Puerra tueda
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$mysqli->query('SET foreign_key_checks = 0');
if ($result = $mysqli->query("SHOW TABLES"))
{
    while($row = $result->fetch_array(MYSQLI_NUM))
    {
        $mysqli->query('DROP TABLE IF EXISTS '.$row[0]);
    }
}

$mysqli->query('SET foreign_key_checks = 1');
$mysqli->close();

//Restaura o Dump
$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', DB_USER, DB_PASSWORD);

$query = file_get_contents($filePath);

$stmt = $db->prepare($query);

if ($stmt->execute())
     echo "Success";
else
     echo "Fail";



?>
