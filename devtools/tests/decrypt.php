<?php

function decrypt($string, $secret_key = 'This is my secret key', $secret_iv = 'This is my secret iv') {
    $output = false;
    $encrypt_method = "AES-256-CBC";

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    // $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

    return $output;
}
$encrypted_txt = "M3QxKzRXN1FaVlg3alZaUDlNbHdOdXVuSnZQNUF5U29XVWpVKzNPSGFFRT0=";

$decrypted_txt = decrypt($encrypted_txt);
echo "Decrypted Text =" .$decrypted_txt. "\n";


echo "\n";
?>
