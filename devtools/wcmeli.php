<?php

ini_set("error_reporting",E_ALL);
error_reporting(E_ALL);
ini_set('display_errors', 1);

include 'decrypt.php';
include 'apimeliwrapperphp/include/all_include.php';
include 'wcmeli_db.php';

$Manage_DB = new Manage_DB();
$meli = new melis();
$meliOrder = new meliOrder();
$meliProduct = new meliProduct();

$secret_Key = 'LuigiRicardoBlackFracalanzaMuzy1';
$secret_Iv  = 'YuriVasconselosDeAlmeidaSa1';
//
// if(isset($_GET['r'])) {
//
// echo file_get_contents('apimeliwrapperphp/include/files/tokens.json');
//
// }

if(isset($_GET['req'])) {

  $getReq = (array)json_decode(base64_decode($_GET['req']));
  if(!isset($getReq['f'])) exit;
  $time = time();
  $from = $getReq['f'];


  if($Manage_DB->is_plugin_free($from)) $version = 'free';
  else $version = 'pay';
  if($Manage_DB->is_blacklisted($from)) {
    echo json_encode(-1);
    exit;
  } else {
    $blacklisted = 'false';
    $Manage_DB->instance_sqlite_free();
    $sql = "INSERT INTO wc_free(website, version, blacklisted, time_stamp) VALUES (?,?,?,?)";
    $Manage_DB->pdo_sqlite_free->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $Manage_DB->pdo_sqlite_free->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $Manage_DB->pdo_sqlite_free->prepare($sql);
    $result->bindParam(1, $from);
    $result->bindParam(2, $version);
    $result->bindParam(3, $blacklisted);
    $result->bindParam(4, $time);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);

    if(isset($getReq['t']) && $getReq['t'] >= time()-3600) {

      $secrets = base64_encode(json_encode(array(base64_encode($secret_Key),base64_encode($secret_Iv))));
      $expiredData = time()+606200;
      $secretsData = $secrets.':'.$expiredData;
      echo $secretsData;
    }
  }
}

if(isset($_GET['gas'])) {

  $getReq = (array)json_decode(base64_decode($_GET['gas']));

  if(isset($getReq['t']) && $getReq['t'] >= time()-3600) {

    $secrets = json_encode(array(base64_encode(APP_ID),base64_encode(SECRET_KEY)));

    echo $secrets;
  }

}

if(isset($_GET['rat'])) {

  $getReq = (array)json_decode(base64_decode($_GET['rat']));

  if(isset($getReq['t']) && $getReq['t'] >= time()-3600) {

    $refreshAccessToken = base64_encode(json_encode($meli->meliRefreshToken($getReq['access_token'],$getReq['refresh_token'])));

    echo $refreshAccessToken;
  }

}

if(isset($_GET['gau'])) {
  $getReq = (array)json_decode(base64_decode($_GET['gau']));

  if(isset($getReq['t']) && $getReq['t'] >= time()-3600) {
    echo $meli->meliGetAuthUrl();
  }
}

if(isset($_POST['query']) || isset($_GET['query'])) {
  if( isset( $_POST['query'] ) ) $wcsa2meli = (array)json_decode(decrypt($_POST['query'],$secret_Key,$secret_Iv),true);
  if( isset( $_GET['query'] ) ) $wcsa2meli  = (array)json_decode(decrypt($_GET['query'],$secret_Key,$secret_Iv),true);

  $time = time();
  $from = $wcsa2meli['f'];
  $sqlite = "sqlite:wc_log.db";

  $pdo = new PDO($sqlite);

  if(isset($wcsa2meli['token'])) {
    $meliOrder->accessToken   = $wcsa2meli['token'];
    $meliProduct->accessToken = $wcsa2meli['token'];
  }
  if(isset($wcsa2meli['product']) || isset($wcsa2meli['description']) || isset($wcsa2meli['images'])) {
    if(isset($wcsa2meli['product'])) {
      $req = json_encode($wcsa2meli['product']);
      $sql = "INSERT INTO log(website, requisicao, time_stamp) VALUES (?,?,?)";
      $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $pdo->prepare($sql);
      $result->bindParam(1, $from);
      $result->bindParam(2, $req);
      $result->bindParam(3, $time);
      $result->execute();
      $select = $result->fetchAll(PDO::FETCH_ASSOC);

      $params = array('access_token' => $meliProduct->accessToken);

      $returnProd = $meliProduct->put("/items/".$wcsa2meli['mlb'], $wcsa2meli['product'], $params);

      // $returnProd = $meliProduct->meliUpdateProduct(, $wcsa2meli['product']);

      if($returnProd['httpCode'] >= 200 && $returnProd['httpCode'] <= 399) $return['product']['success'] = true;
      else {
        foreach ($returnProd['body']->cause as $key => $value) {
          $return['product']['MessageError'][] = $value->message;

        }
      }
    }
    if(isset($wcsa2meli['description'])) {
      $req = json_encode($wcsa2meli['description']);
      $sql = "INSERT INTO log(website, requisicao, time_stamp) VALUES (?,?,?)";
      $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $pdo->prepare($sql);
      $result->bindParam(1, $from);
      $result->bindParam(2, $req);
      $result->bindParam(3, $time);
      $result->execute();
      $select = $result->fetchAll(PDO::FETCH_ASSOC);

      $returnDesc = $meliProduct->meliUpdateProductDescription($wcsa2meli['mlb'], $wcsa2meli['description']);

      if($returnDesc['httpCode'] >= 200 && $returnDesc['httpCode'] <= 399) $return['description']['success'] = true;
      else {
        foreach ($returnProd['body']->cause as $key => $value) {
          $return['description']['MessageError'][] = $value->message;
        }
      }
    }
    if(isset($wcsa2meli['images'])) {
      if($Manage_DB->is_plugin_free($from)) {
        echo json_encode(-1);
        exit;
      }
      $req = json_encode($wcsa2meli['images']);
      $sql = "INSERT INTO log(website, requisicao, time_stamp) VALUES (?,?,?)";
      $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $pdo->prepare($sql);
      $result->bindParam(1, $from);
      $result->bindParam(2, $req);
      $result->bindParam(3, $time);
      $result->execute();
      $select = $result->fetchAll(PDO::FETCH_ASSOC);

      $updateProductImages = $meliProduct->meliUpdateProductImages($wcsa2meli['mlb'], $wcsa2meli['images']);

      if($updateProductImages['httpCode'] >= 200 && $updateProductImages['httpCode'] <= 399) $return['images']['success'] = true;
      else {
        foreach ($returnProd['body']->cause as $key => $value) {
          $return['images']['MessageError'][] = $value->message;
        }
      }
    }
    if(isset($return)) echo json_encode($return);
  }


  if(isset($wcsa2meli['order'])) {
    if($Manage_DB->is_plugin_free($from)) {
      echo json_encode(-1);
      exit;
    }
    $req = json_encode($wcsa2meli['order']);
    $sql = "INSERT INTO log(website, requisicao, time_stamp) VALUES (?,?,?)";
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $pdo->prepare($sql);
    $result->bindParam(1, $from);
    $result->bindParam(2, $req);
    $result->bindParam(3, $time);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);

    if(is_array($wcsa2meli['order']['id'])) {
      foreach ($wcsa2meli['order']['id'] as $key => $value) {
         $returnOrder = $meliOrder->meliGetOrder($value);
         if($returnOrder['httpCode'] >= 200 && $returnOrder['httpCode'] <= 399) $return['order'][] = $returnOrder['body'];
         else $return['order']['MessageError'][] = $returnOrder['body']->message;
       }
    } else {
      $returnOrder = $meliOrder->meliGetOrder($wcsa2meli['order']['id']);
      if($returnOrder['httpCode'] >= 200 && $returnOrder['httpCode'] <= 399) $return['order'][] = $returnOrder['body'];
      else {
          $return['order']['MessageError'][] = $returnOrder['body']->message;
      }
    }



    if(isset($return)) echo json_encode($return);
  }

  if(isset($wcsa2meli['label'])) {
    if($Manage_DB->is_plugin_free($from)) {
      echo json_encode(-1);
      exit;
    }
    $curl_url =  "https://api.mercadolibre.com/shipment_labels?shipment_ids=".$wcsa2meli['label']['shipment_ids']."&response_type=pdf&access_token=".$wcsa2meli['token'];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_URL, $curl_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $content = curl_exec($ch);

    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    if($httpCode >= 200 && $httpCode <= 399) {
      $return = $content;
      echo $return;
    } else {
      $content = (array)json_decode($content,true);
      $return['label']['MessageError'] = $content['message'];
      echo json_encode($return);
    }
  }
}
if(isset($_POST['q'])) {
  $wcsa2meli = (array)json_decode(base64_decode($_POST['q']),true);

  if(isset($wcsa2meli['context'])) {
    $telemetria = json_decode(base64_decode($wcsa2meli['context']['c']),true);
    $from = base64_decode($wcsa2meli['context']['f']);
    $sqlite = "sqlite:wc_telemetria.db";
    $time = time();
    $users = json_encode($telemetria['users']);
    $products = json_encode($telemetria['products']);
    $orders = json_encode($telemetria['orders']);
    $active_plugins = json_encode($telemetria['active_plugins']);

    $pdo = new PDO($sqlite);
    $sql = "INSERT INTO telemetria(users,products,orders,address,city,country,postcode,admin_email,active_plugins,time_stamp) VALUES (?,?,?,?,?,?,?,?,?,?)";
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $pdo->prepare($sql);
    $result->bindParam(1, $users);
    $result->bindParam(2, $products);
    $result->bindParam(3, $orders);
    $result->bindParam(4, $telemetria['address']);
    $result->bindParam(5, $telemetria['city']);
    $result->bindParam(6, $telemetria['country']);
    $result->bindParam(7, $telemetria['postcode']);
    $result->bindParam(8, $telemetria['adminemail']);
    $result->bindParam(9, $active_plugins);
    $result->bindParam(10,$time);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);
    echo "true";
  }
}

if(isset($_POST['gpl'])) {
  $wcsa2meli  = (array)json_decode(decrypt($_POST['gpl'],$secret_Key,$secret_Iv),true);

  if(isset($wcsa2meli['f'])) {
    $productList = json_encode($wcsa2meli['productList']);
    $sqlite = "sqlite:wc_importer.db";
    $from = $wcsa2meli['f'];
    $time = time();
    $pdo = new PDO($sqlite);

    $sql = "SELECT products FROM importer WHERE website=?";
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $pdo->prepare($sql);
    $result->bindParam(1, $from);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);

    if(empty($select)) {
      $sql = "INSERT INTO importer(website,products,time_stamp) VALUES (?,?,?)";
      $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $pdo->prepare($sql);
      $result->bindParam(1, $from);
      $result->bindParam(2, $productList);
      $result->bindParam(3, $time);
      $result->execute();
      $return = $result->fetchAll(PDO::FETCH_ASSOC);
    }
    // if(isset($select[0]['products']) && empty($select[0]['products'])) {
    //   $sql = "UPDATE importer SET products = ? , time_stamp = ? WHERE website = ?";
    //   $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    //   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //   $result = $pdo->prepare($sql);
    //   $result->bindParam(1, $productList);
    //   $result->bindParam(2, $time);
    //   $result->bindParam(3, $from);
    //   $result->execute();
    //   $return = $result->fetchAll(PDO::FETCH_ASSOC);
    // }
    if(isset($select[0]['products']) && !empty($select[0]['products'])) {
      $productList = json_encode(array_merge($wcsa2meli['productList'],json_decode($select[0]['products'],true)));
      $sql = "UPDATE importer SET products = ? , time_stamp = ? WHERE website = ?";
      $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $pdo->prepare($sql);
      $result->bindParam(1, $productList);
      $result->bindParam(2, $time);
      $result->bindParam(3, $from);
      $result->execute();
      $return = $result->fetchAll(PDO::FETCH_ASSOC);
    }
    if(isset($wcsa2meli['token'])) {
      $meliProduct->accessToken = $wcsa2meli['token'];
    }

    $params = array('access_token' => $meliProduct->accessToken);
    foreach ($wcsa2meli['productList'] as $key => $value) {
      if($key != 'select' && $key != 'select-all') {
        $return = $meliProduct->get("/items/".$value, $params);

        $returnProd = [
          'id' => $return['body']->id,
          'title' => $return['body']->title,
          'price' => $return['body']->price,
          'available_quantity' => $return['body']->available_quantity,
        ];

        if($return['httpCode'] >= 200 && $return['httpCode'] <= 399) $returnP = $returnProd;
        else $returnP['MessageError'] = $return['body']->message;

        $returnProdDesc = $meliProduct->meliGetProductDescription($value);

        if($returnProdDesc['httpCode'] >= 200 && $returnProdDesc['httpCode'] <= 399) $returnP['description'] = $returnProdDesc['body']->plain_text;
        else $returnP['MessageError'] = $returnProdDesc['body']->message;
        $infoProducts[] = $returnP;
        // print_r($infoProducts);
        // exit;
      }
    }

    echo json_encode($infoProducts);
  }
}

if(isset($_POST['gap'])) {
  $wcsa2meli  = (array)json_decode(decrypt($_POST['gap'],$secret_Key,$secret_Iv),true);

  if(isset($wcsa2meli['f'])) {
    $productList = json_encode($wcsa2meli['productList']);
    $sqlite = "sqlite:wc_importer.db";
    $from = $wcsa2meli['f'];
    $time = time();
    $pdo = new PDO($sqlite);

    $sql = "UPDATE importer SET products = ?, time_stamp = ? WHERE website = ?";
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $pdo->prepare($sql);
    $result->bindParam(1, $productList);
    $result->bindParam(2, $time);
    $result->bindParam(3, $from);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);

    if(isset($wcsa2meli['token'])) {
      $meliProduct->accessToken = $wcsa2meli['token'];
    }

    $params = array('access_token' => $meliProduct->accessToken);

    foreach ($wcsa2meli['productList'] as $key => $value) {
      $returnProd = $meliProduct->get("/items/".$value, $params);

      if($returnProd['httpCode'] >= 200 && $returnProd['httpCode'] <= 399) $return = $returnProd['body'];
      else $return['MessageError'][] = $returnProd['body']->message;

      $returnProdDesc = $meliProduct->meliGetProductDescription($value);

      if($returnProdDesc['httpCode'] >= 200 && $returnProdDesc['httpCode'] <= 399) $return->description = $returnProdDesc['body']->plain_text;
      else $return['MessageError'][] = $returnProdDesc['body']->message;

      $infoProducts[] = $return;
    }

    echo json_encode($infoProducts);
  }
}
if(isset($_POST['wccpl'])) {
  $wcsa2meli  = (array)json_decode(decrypt($_POST['wccpl'],$secret_Key,$secret_Iv),true);

  if(isset($wcsa2meli['f'])) {
    $productList = json_encode($wcsa2meli['productList']);
    $sqlite = "sqlite:wc_importer_to_ml.db";
    $from = $wcsa2meli['f'];
    $time = time();
    $pdo = new PDO($sqlite);

    $sql = "SELECT products FROM importer WHERE website=?";
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $pdo->prepare($sql);
    $result->bindParam(1, $from);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);

    if(empty($select)) {
      $sql = "INSERT INTO importer(website,products,time_stamp) VALUES (?,?,?)";
      $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $pdo->prepare($sql);
      $result->bindParam(1, $from);
      $result->bindParam(2, $productList);
      $result->bindParam(3, $time);
      $result->execute();
      $return = $result->fetchAll(PDO::FETCH_ASSOC);
    }

    if(isset($select[0]['products']) && strlen($select[0]['products']) > 10) {
      $productList = json_encode(array_merge($wcsa2meli['productList'],json_decode($select[0]['products'],true)));
      $sql = "UPDATE importer SET products = ? , time_stamp = ? WHERE website = ?";
      $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $result = $pdo->prepare($sql);
      $result->bindParam(1, $productList);
      $result->bindParam(2, $time);
      $result->bindParam(3, $from);
      $result->execute();
      $return = $result->fetchAll(PDO::FETCH_ASSOC);
    }
    if(isset($wcsa2meli['token'])) {
      $meliProduct->accessToken = $wcsa2meli['token'];
    }

    $params = array('access_token' => $meliProduct->accessToken);
    foreach ($wcsa2meli['productList'] as $key => $value) {
      $return = $meliProduct->meliCreateProduct($value);
      if(isset($return['body']->error)) :
        foreach ($return['body']->cause as $_key => $_value) :
          if($_value->code == 'item.attributes.missing_required') :
            $body = [
                "title" => $value['title'],
                "categoryId" => 'MLB3530',
                "price" => $value['price'],
                "qty" => $value['qty'],
                "description" => $value['description'],
                'images' => $value['images'],
                'brand' => $value['brand'],
                'sku' => $value['sku']
            ];

            $return = $meliProduct->meliCreateProduct($body);

            if(!isset($return['body']->error)) :
              $defaultCategory = true;
            endif;
          endif;
        endforeach;
      endif;
      if(isset($return['out_stock'])) $meliProduct->meliUpdateProduct($return['body']->id,['available_quantity' => 0]);

      if(!isset($return['body']->error)) :
        if(isset($defaultCategory)) :
          if(isset($return['body']->id)) :
            $returnP['createProducts'][] = ['wcid'=>$value['wcid'],'wcsku'=>$value['sku'],'id'=>$return['body']->id,'name' => $return['body']->title,'defaultCategory'=>true];
          else :
            $returnP['MessageError'][] = ['produto'=>$value['title'],'erro'=>$return['body']->cause[0]->message];
          endif;
        else :
          if(isset($return['body']->id)) :
            $returnP['createProducts'][] = ['wcid'=>$value['wcid'],'wcsku'=>$value['sku'],'id'=>$return['body']->id,'name' => $return['body']->title,'defaultCategory'=>true];
          else :
            $returnP['MessageError'][] = ['produto'=>$value['title'],'erro'=>$return['body']->cause[0]->message];
          endif;
        endif;
      else :
        $returnP['MessageError'][] = ['produto'=>$value['title'],'erro'=>$return['body']->cause[0]->message];
      endif;
    }
    echo json_encode($returnP);
  }
}
if(isset($_POST['wccap'])) {
  $wcsa2meli  = (array)json_decode(decrypt($_POST['wccap'],$secret_Key,$secret_Iv),true);

  if(isset($wcsa2meli['f'])) {
    $productList = json_encode($wcsa2meli['productList']);
    $sqlite = "sqlite:wc_importer_to_ml.db";
    $from = $wcsa2meli['f'];
    $time = time();
    $pdo = new PDO($sqlite);

    $sql = "UPDATE importer SET products = ? , time_stamp = ? WHERE website = ?";
    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $pdo->prepare($sql);
    $result->bindParam(1, $productList);
    $result->bindParam(2, $time);
    $result->bindParam(3, $from);
    $result->execute();
    $return = $result->fetchAll(PDO::FETCH_ASSOC);

    if(isset($wcsa2meli['token'])) {
      $meliProduct->accessToken = $wcsa2meli['token'];
    }

    $params = array('access_token' => $meliProduct->accessToken);
    foreach ($wcsa2meli['productList'] as $key => $value) {
      $return = $meliProduct->meliCreateProduct($value);

      if(!isset($return['body']->error) && isset($return['body']->id)) $returnP['createProducts'][] = ['wcid'=>$value['wcid'],'wcsku'=>$value['sku'],'id'=>$return['body']->id,'name' => $return['body']->title];
      else $returnP['MessageError'][] = ['produto'=>$value['title'],'erro'=>$return['body']->cause[0]->message];
    }
    echo json_encode($returnP);
  }
}

if(isset($_POST['upfull'])) {
  $wcsa2meli  = (array)json_decode(decrypt($_POST['upfull'],$secret_Key,$secret_Iv),true);

  if(isset($wcsa2meli['f'])) {
    $sqlite = "sqlite:wc_access.db";
    $from = $wcsa2meli['f'];
    $time = time();
    $access = true;
    $pdo = new PDO($sqlite);

    if(!$Manage_DB->is_client($from)) {
      echo json_encode('-1');
      exit;
    }

    if(!$Manage_DB->is_plugin_pay($from)) {
      echo json_encode('-1');
      exit;
    }

    $wcmeli_order                 = file_get_contents('files_full_version/wcmeli_order.txt');
    $wcmeli_product_images        = file_get_contents('files_full_version/wcmeli_product_images.txt');
    $wcmeli_grouped_product       = file_get_contents('files_full_version/wcmeli_grouped_product.txt');
    $wcmeli_external_product      = file_get_contents('files_full_version/wcmeli_external_product.txt');
    $wcmeli_variable_product      = file_get_contents('files_full_version/wcmeli_variable_product.txt');
    $wcmeli_order_questions_sales = file_get_contents('files_full_version/wcmeli_order_questions_sales.txt');

    $returnP['upfull'] = [
      'o' => $wcmeli_order,
      'pi' => $wcmeli_product_images,
      'gp' => $wcmeli_grouped_product,
      'ep' => $wcmeli_external_product,
      'vp' => $wcmeli_variable_product,
      'oqs' => $wcmeli_order_questions_sales
    ];
    echo json_encode($returnP);
  }
}
?>
