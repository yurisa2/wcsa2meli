<?php

ini_set("error_reporting",E_ALL);
error_reporting(E_ALL);
ini_set('display_errors', 1);
class Manage_DB
{
 /*	website	TEXT,
 	* requisicao	TEXT,
 	* timestamp	INTEGER
  */
  static $log            = "sqlite:wc_log.db";

 /*
  * website	TEXT,
 	* access	TEXT,
 	* time_stamp	INTEGER
  */
  static $access         = "sqlite:wc_access.db";

 /*
	* website	TEXT,
	* products	TEXT,
	* time_stamp	INTEGER
  */
  static $importer       = "sqlite:wc_importer.db";

 /* website	TEXT,
  * users	TEXT,
  * products	TEXT,
  * orders	TEXT,
  * address	TEXT,
  * city	TEXT,
  * country	TEXT,
  * postcode	TEXT,
  * admin_email	TEXT,
  * active_plugins	TEXT,
  * time_stamp	INTEGER
  */
  static $telemetria     = "sqlite:wc_telemetria.db";

 /*
  * website	TEXT,
	* products	TEXT,
	* time_stamp	INTEGER
  */
  static $importer_to_ml = "sqlite:wc_importer_to_ml.db";

 /*
  * website	TEXT,
  * version TEXT,
	* time_stamp	INTEGER
  */
  static $free = "sqlite:wc_free.db";

  public function __construct()
  {

  }

  public function instance_sqlite_log()
  {
    $this->pdo_sqlite_log = new PDO(self::$log);
  }

  public function instance_sqlite_users()
  {
    $this->pdo_sqlite_users = new PDO(self::$users);
  }

  public function instance_sqlite_importer()
  {
    $this->pdo_sqlite_importer = new PDO(self::$importer);
  }

  public function instance_sqlite_access()
  {
    $this->pdo_sqlite_access = new PDO(self::$access);
  }

  public function instance_sqlite_telemetria()
  {
    $this->pdo_sqlite_telemetria = new PDO(self::$telemetria);
  }

  public function instance_sqlite_importer_to_ml()
  {
    $this->pdo_sqlite_importer_to_ml = new PDO(self::$importer_to_ml);
  }

  public function instance_sqlite_free()
  {
    $this->pdo_sqlite_free = new PDO(self::$free);
  }

  public function is_plugin_free($from)
  {
    $this->instance_sqlite_access();

    $sql = "SELECT access FROM wc_access WHERE website=?";
    $this->pdo_sqlite_access->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $this->pdo_sqlite_access->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $this->pdo_sqlite_access->prepare($sql);
    $result->bindParam(1, $from);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);

    if(empty($select)) return true;

    return false;
  }

  public function is_plugin_pay($from)
  {
    $this->instance_sqlite_access();

    $sql = "SELECT access FROM wc_access WHERE website=?";
    $this->pdo_sqlite_access->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $this->pdo_sqlite_access->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $this->pdo_sqlite_access->prepare($sql);
    $result->bindParam(1, $from);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);

    if(empty($select) || $select[0]['access'] == 'false') return false;

    return true;
  }

  public function is_blacklisted($from)
  {
    $this->instance_sqlite_free();

    $sql = "SELECT blacklisted FROM wc_free WHERE website=?";
    $this->pdo_sqlite_free->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $this->pdo_sqlite_free->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $this->pdo_sqlite_free->prepare($sql);
    $result->bindParam(1, $from);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);

    if(empty($select) || $select[0]['blacklisted'] == 'false') return false;

    return true;
  }

  public function is_client($from)
  {
    $this->instance_sqlite_access();

    $sql = "SELECT id FROM wc_access WHERE website=?";
    $this->pdo_sqlite_access->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $this->pdo_sqlite_access->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $this->pdo_sqlite_access->prepare($sql);
    $result->bindParam(1, $from);
    $result->execute();
    $select = $result->fetchAll(PDO::FETCH_ASSOC);

    if(empty($select)) return false;

    return true;
  }
}
// $Manage_DB = new Manage_DB;
// var_dump($Manage_DB->is_blacklisted('http://localhost/wcsa2meli/wordpress'));
?>
