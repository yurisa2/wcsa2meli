<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wcsa2meli' );

/** MySQL database username */
define( 'DB_USER', 'wcsa2meli' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wcsa2meli' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'A/&^?ReJzRe}_GQz<rJbwx*X/==j+9ZCIN)o]Uv_y+`]%w+z{NrxvI^TGk{(32;q' );
define( 'SECURE_AUTH_KEY',  'R)!y`tGg(j:c&*-HHXUEzA/%-OU2$$OGu?po<C73dk=`[:/w_CZk0*wkBXRcK*v%' );
define( 'LOGGED_IN_KEY',    'ij9CDn^g9-9ec5Eaf:u|<Rh99Y)><?R1RP!w*ljrRIrVTdQ&E(gKYK$W4OV]d)OS' );
define( 'NONCE_KEY',        '[+k,RUyaMHir67:q6K?R$?*jvQj,_qRqd3NZ9;$<%`x=li%ecT~e@?$S|G_i,Sg?' );
define( 'AUTH_SALT',        'lRu${Q0tePmT)PbTbaBZt!0wyHByb+Qsi~,{kxL2Qa_Zc:ldcAknY.E|iEQN?NT,' );
define( 'SECURE_AUTH_SALT', '1,XS>j8CKZoO4l&^?0QlN#j17mE%FH*r,%y:N&#f}DJZZ13=O%v-%4rgZuJR]z5;' );
define( 'LOGGED_IN_SALT',   'CN1aWzrNs6h#P;H iwS1Ow=rcWfkwQx1MB2I826q@o%$.~iSY9D8Zb@Mu$PIeB9+' );
define( 'NONCE_SALT',       'ff,jH `?&-92C#Svp/:dp|o!v+)EhY<Eg|5NUVPr>^R5pliGb*C$GAHw[Rnop&4)' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
