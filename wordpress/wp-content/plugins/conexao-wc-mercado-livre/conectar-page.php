<?php
  function wcmeli_custom_query_vars_filter_conectar($vars) {
    $vars[] .= 'post_type';
    return $vars;
  }
  add_filter( 'query_vars', 'wcmeli_custom_query_vars_filter_conectar' );

  if(!class_exists('flux')) include WCMELI_PLUGIN_PATH.'wcml_v2/include/sa2_flux/include/flux.php';

  $flux = new flux('wcmeli_product');


  if(!file_exists(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')) file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode([])));
  $wcxml = (array)json_decode($flux->fluxiCode(file_get_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')),true);
  if(!class_exists('meliProduct')) require 'wcml_v2/include/all_include.php';

  $wcmeliProduct = new meliProduct;

  if(isset($_POST['prod'])) :
    $wcProdId = $_POST['wcprod'];
    $mlProdId = $_POST['prod'];
    $wcProdInfo = wc_get_product($wcProdId);

    if(!$wcProdInfo->get_sku()) :
      $wcProdIdType = 'id';
    else :
      $wcProdId = $wcProdInfo->get_sku();
      $wcProdIdType = 'sku';
    endif;

    // $mlProdSku = $wcmeliProduct->meliGetProductSku($mlProdId);
    //
    // if(!$mlProdInfo) :
    //   $mlProdIdType = 'id';
    // else :
    //   $mlProdId = $mlProdSku;
    //   $mlProdIdType = 'sku';
    // endif;

    $wcxml[] = [
      'id' => end($wcxml)['id']+1,
      'wc' => $wcProdId,
      'wc_type' => $wcProdIdType,
      'mlb' => $mlProdId,
      'mlb_type' => 'id'
    ];
     file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode($wcxml)));
     header( "Location: edit.php?post_type=product" );
  endif;

  if(isset($_POST['createprod'])) : ?>
  <div class="wrap" id="unique">
    <h1 class="wcmeli_title"><?php echo __( 'Conectar Produto'); ?></h1>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <div id="all-products" style="line-height:50px;">

    <?php $wcProdId = $_POST['wcprod'];

    $wcProdInfo = wc_get_product($wcProdId);

    if(!$wcProdInfo->get_sku()) :
      $wcProdIdType = 'id';
      $wcProdSku = false;
    else :
      $wcProdSku = $wcProdInfo->get_sku();
      $wcProdIdType = 'sku';
    endif;

    $productName = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/"),explode(" ","a A"),$wcProdInfo->get_title());
    if(strlen($productName) > 60) {
      $productName = explode(' ',$productName);
      array_pop($productName);
      $productName = implode(' ',$productName);
    }
    $title = WCMELI_PREFFIX_PROD.trim($productName);

    $settingPrice = ($wcProdInfo->get_price() * WCMELI_SETTINGS_PRICE_MULTIPLICATION) + WCMELI_SETTINGS_PRICE_ADDITION;
    $price = round($wcProdInfo->get_price() + ($wcProdInfo->get_price() * WCMELI_COMMISSION) + $settingPrice ,2);

    $stock = floor($wcProdInfo->get_stock_quantity() + ($wcProdInfo->get_stock_quantity()*WCMELI_SETTINGS_STOCK));

    if($stock < 0 || $wcProdInfo->get_stock_status() == 'onbackorder') $stock = 0;
    if($stock == 0 && $wcProdInfo->get_stock_status() == 'instock') $stock = 1;

    $description = $wcProdInfo->get_description();

    if(strlen(wp_get_attachment_url($wcProdInfo->get_image_id())) < 2) :
      $image_url = 'https://sa2.com.br/wcsa2meli/defaul_image.png';
    else :
      $image_url = wp_get_attachment_url($wcProdInfo->get_image_id());
    endif;

    $categoryid = $wcmeliProduct->meliGetProductCategory($title)['body']->id;

    if($wcProdSku) $sku = $wcProdSku;
    else $sku = '';

    if($stock == 0) $qty = 1;
    else $qty = $stock;
    $body[] = [
        'wcid' => $wcProdId,
        "title" => $title,
        "categoryId" => $categoryid,
        "price" => $price,
        "qty" => $qty,
        "description" => $description,
        "images" => [
          "source" => $image_url
        ],
        'brand' => WCMELI_BRAND,
        'sku' => $sku
    ];
    // echo "<pre>";
    // var_dump($body);
    // exit;
    // $return = $wcmeliProduct->meliCreateProduct($body);

    // if(isset($return['body']->error)) :
    //   foreach ($return['body']->cause as $key => $value) :
    //     if($value->code == 'item.attributes.missing_required') :
    //       $body = [
    //           "title" => $title,
    //           "categoryId" => 'MLB3530',
    //           "price" => $price,
    //           "qty" => $qty,
    //           "description" => $description,
    //           "images" => [
    //             "source" => $image_url
    //           ],
    //           'brand' => WCMELI_BRAND,
    //           'sku' => $sku
    //       ];
    //       $return = $wcmeliProduct->meliCreateProduct($body);
    //       if(!isset($return['body']->error)) :
    //         $defaultCategory = true;
    //       endif;
    //     endif;
    //   endforeach;
    // endif;
    // if(!isset($return['body']->error)) :
    //   if($stock == 0) $wcmeliProduct->meliUpdateProduct($return['body']->id,['available_quantity'=>0]);
    //   if(!$wcProdSku) :
    //     $wcProdId = $wcProdId;
    //     // $mlProdIdType = 'id';
    //     // $mlProdId = $return['body']->id;
    //   else :
    //     // $mlProdIdType = 'sku';
    //     // $mlProdId = $sku;
    //     $wcProdId = $wcProdSku;
    //   endif;
    //   $wcxml[] = [
    //     'id' => end($wcxml)['id']+1,
    //     'wc' => $wcProdId,
    //     'wc_type' => $wcProdIdType,
    //     'mlb' => $return['body']->id,
    //     'mlb_type' => 'id'
    //   ];
    // endif;
    include 'wcml_v2/includes/wcmeli_get.php';
    if(isset($return)) {
      $productAttributes['productList'] = $body;
      $productAttributes['token'] = $wcmeliProduct->accessToken;
      $productAttributes['f'] = get_site_url();
      $productData = $wcmeliProduct->updatewcsa2meli(json_encode($productAttributes),base64_decode($return[0]),base64_decode($return[1]));

      $body = array(
          'wccpl' => $productData
      );

      $args = array(
          'body' => $body,
          'timeout' => '600',
          'redirection' => '5',
          'httpversion' => '1.0',
          'blocking' => true,
          'headers' => array(),
          'cookies' => array()
      );
      $url = 'https://sa2.com.br/wcsa2meli/wcmeli.php';
      $result = wp_remote_retrieve_body(wp_remote_post($url, $args));
      // echo "<pre>";
      // var_dump($result);
      // exit;
      $result = (array)json_decode($result,true);
      if(isset($result['createProducts'])) :
        $defaultCategoryProduct = 0;
        foreach ($result['createProducts'] as $key => $value) :
          if(!$value['wcsku']) :
            $wcxml[] = [
              'id' => end($wcxml)['id']+1,
              'wc' => $value['wcid'],
              'wc_type' => 'id',
              'mlb' => $value['id'],
              'mlb_type' => 'id'
            ];
            file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode($wcxml)));
          else :
            $wcxml[] = [
              'id' => end($wcxml)['id']+1,
              'wc' => $value['wcsku'],
              'wc_type' => 'sku',
              'mlb' => $value['id'],
              'mlb_type' => 'id'
            ];
            file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode($wcxml)));
          endif;

          if(isset($value['defaultCategory'])) :
            add_action( 'admin_notices', 'wcmeliDefaultCategoryProduct' );
          endif;
          if(isset($value['defaultCategory'])) : ?>
            <?php $url = 'https://www.mercadolivre.com.br/publicaciones/'.$value['id'].'/modificar' ; ?>

            <h1 class="wcmeli_title" style="color:red;font-size:1.4rem;text-transform:uppercase">O produto <?php echo $value['name']; ?> foi criado em uma categoria genérica.</h1>
            <?php if($defaultCategoryProduct == 0) : ?>
              <span class="wcmeli_title title">A categoria recomendada pelo Mercado Livre para esse produto necessita de informações adicionais.</span><br>
              <span class="wcmeli_title title">Favor verifique a categoria no Mercado Livre e altere para uma que seja mais correta.</span><br>
            <?php endif; ?>
            <a target="_blank" href="<?php echo $url; ?>">Ver no Mercado Livre</a>
          <?php else :
            header( "Location: edit.php?post_type=product" );
          endif;
          $created[] = $value['name'];
          $defaultCategoryProduct++;
        endforeach;
      endif;
      if(isset($result['MessageError'])) :
        foreach ($result['MessageError'] as $key => $value) :
          add_settings_error( 'sa2meliproduct_error'.$key, 'sa2meliproduct_error', __( 'O produto '.$value['produto'].' não foi criado. '.$value['erro'] ) );
          settings_errors( 'sa2meliproduct_error'.$key );
        endforeach;
      endif; ?>
    }
  </div>
</div>
</div>
</div>
</div>
  <?php endif;

  if(isset($_GET['action'])) :
    if($_GET['action'] == 'desconectar') :
      $productId = $_GET['wcprod'];
      $wcProdInfo = wc_get_product($productId);
      if(!$wcProdInfo->get_sku()) $productId = $productId;
      else $productId = $wcProdInfo->get_sku();
      if(!file_exists(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')) file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode([])));
      $wcxml = (array)json_decode($flux->fluxiCode(file_get_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')),true);
      foreach ($wcxml as $key => $value) :
        if($value['wc'] != $productId) $newWcxml[] = $value;
      endforeach;
      file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode($newWcxml)));
      header( "Location: edit.php?post_type=product" );
    endif;
  endif;

  if(isset($_GET['action'])) :
    if($_GET['action'] == 'conectar') :
      $params = array('post_type' => 'product','page' => 'conectar');
      $productIds = $wcmeliProduct->meliGetProducts();
      // echo "<pre>";
      // var_dump($productIds);
      // exit;
      $wcId = $_GET['wcprod'];
      $wcProdInfo = wc_get_product($wcId); ?>

      <div class="wrap" id="unique">
        <h1 class="wcmeli_title"><?php echo __( 'Conectar Produto'); ?></h1>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                  <?php if($wcProdInfo->get_type() == 'simple') : ?>
                    <button type="button" style="width:100%;height:50px;margin-top:20px;" name="submit" id="show" class="button button-primary" value="">Produto existente no Mercado Livre</button>
                    <div class="hidden-prod" style="display:none;border:1px solid #0062CC;margin:10px;padding:10px;">
                      <form action="<?php echo add_query_arg($params, 'edit.php'); ?>" method="post">
                      <?php foreach ($productIds as $key => $value) : ?>
                        <?php $productInfo = $wcmeliProduct->meliGetProduct($value);
                        if(!is_null($productInfo['body'])) :?>
                          <?php $found = false; ?>
                          <?php foreach ($wcxml as $key => $_value) : ?>
                            <?php if($productInfo['body']->id == $_value['mlb']) $found = true; ?>
                          <?php endforeach; ?>
                          <?php if(!$found) : ?>
                            <?php $hasProduct = true; ?>
                            <input type="hidden" name="wcprod" value="<?php echo $wcId; ?>">
                            <input type="radio" name="prod" value="<?php echo __($value); ?>"/>
                            <a href="<?php echo $productInfo["body"]->permalink; ?>" target="_blank">
                              <?php if(is_int(strpos(strtolower($productInfo['body']->title),strtolower($wcProdInfo->get_title())))) : ?>
                               <strong><span class="wcmeli_title"><?php echo __( $productInfo["body"]->title ); ?></span></strong><br>
                             <?php else : ?>
                                <span class="wcmeli_title"><?php echo __( $productInfo["body"]->title ); ?></span><br>
                             <?php endif; ?>
                           </a>
                          <?php endif; ?>
                        <?php //else : ?>
                          <!-- <span class="wcmeli_title"><?php echo __('Problemas com o Mercado Livre. Recarregue a página!'); ?></span> -->
                        <?php endif; ?>
                      <?php endforeach; ?>
                      <?php if($hasProduct) : ?>
                        <input type="submit" name="conectar" id="conectar" class="button button-large button-primary" value="Conectar" style="width:49%">
                      <?php else : ?>
                        <div id="all-products">
                          <h1 class="wcmeli_title title"><?php echo __( 'TODOS OS PRODUTOS DO MERCADO LIVRE ESTÃO CONECTADOS'); ?></h1>
                        </div>
                      <?php endif; ?>
                    </form>
                    </div>
                    <form action="<?php echo add_query_arg($params, 'edit.php'); ?>" method="post">
                      <input type="hidden" name="wcprod" value="<?php echo $wcId; ?>">
                      <button type="submit" style="width:100%;height:50px;margin-top:20px;" name="createprod" id="createprod" class="button button-primary" value="">Produto não existente no Mercado Livre</button>
                    </form>
                  <?php else : ?>
                    <div id="all-products" class="painel nonquestions">
                      <h1 class="wcmeli_title title"><?php echo __( 'PRODUTO SELECIONADO NÃO CORRESPONDE AO TIPO DE PRODUTO SIMPLES. APENAS PRODUTOS SIMPLES SÃO CRIADOS E SINCRONIZADOS'); ?></h1>
                    </div>
                  <?php endif; ?>
                </div>
              </div>
            </div>
        </div>
    <?php endif; ?>
  <?php endif; ?>
  <script>
  var jqNoConflict = jQuery.noConflict();
  jqNoConflict(document).ready(function(){
    jqNoConflict("#show").click(function(){
      jqNoConflict('.hidden-prod').toggle();
    });
  });
  </script>
