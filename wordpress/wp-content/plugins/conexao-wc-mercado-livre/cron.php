<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_filter( 'cron_schedules', 'add_cron_30_secs' );

function add_cron_30_secs( $schedules ) {
    $schedules['thirty_seconds'] = array(
        'interval' => 30,
        'display'  => esc_html__( 'Every Thirty Seconds' ),
    );

    return $schedules;
}

add_action( 'cron_hook', 'run_sa2_cron' );

wp_next_scheduled( 'cron_hook' );

if ( ! wp_next_scheduled( 'cron_hook' ) ) {
    wp_schedule_event( time(), 'thirty_seconds', 'cron_hook' );
}

function run_sa2_cron() {
  include_once WCMELI_PLUGIN_PATH.'wcml_v2/script.php';
}

?>
