<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

class DB_WCMELI_Manager
{
  protected $option_column = 'config_wc_meli';
  public function __construct() {
    $options = $this->wcsa2meliGetOptions();
    if(!$options) {
      $configOptions = [
        'addprice'              => 0.0,
        'commission'            => 0,
        'errortime'             => 60,
        'settingsstock'         => 20,
        'preffixProd'            => '',
        'suffixprod'            => 'Marca dos Produtos',
        'prodbrand'             => 'Marca dos Produtos',
        'multprice'             => 0,
        'questiontime'          => 60,
        'syncimages'            => 1440,
        'configmail'            => true,
        'emailproductquestion'  => 'example@mail.com,example@mail.com',
        'emailaftersale'        => 'example@mail.com,example@mail.com',
        'emailnewsale'          => 'example@mail.com,example@mail.com',
        'emailerror'            => 'example@mail.com,example@mail.com',
        'prodquestion'          => true,
        'autoprodquestionfrete' => false,
        'syncprod'              => true,
        'attprodtitle'          => true,
        'attprodprice'          => true,
        'attproddesctiption'    => true,
        'attprodstock'          => true,
        'attprodimages'         => true,
        'syncorder'             => true,
        'smtp'                  => false,
        'host'                  => 'host.host.host',
        'auth'                  => true,
        'username'              => 'username',
        'password'              => 'password',
        'secure'                => 'tls',
        'from_mail'             => 'example@mail.com',
        'from_title'            => 'WcMeli Sincronizador Automático',
        'token'                 => null,
        'userid'                => '',
        'agree'                 => false
      ];
      add_option($this->option_column,serialize($configOptions));
    }
    $this->config_sa2_meli = $this->wcsa2meliGetOptions();
  }

  public function wcsa2meliGetAdvancedOptionsForm()
  {
    if(isset($_POST['addprice'])) $this->config_sa2_meli['addprice'] = (float)sanitize_text_field($_POST['addprice']);
    if(isset($_POST['commission'])) $this->config_sa2_meli['commission'] = (float)sanitize_text_field($_POST['commission']);
    if(isset($_POST['errortime'])) $this->config_sa2_meli['errortime'] = (int)sanitize_text_field($_POST['errortime']);
    if(isset($_POST['settingsstock'])) $this->config_sa2_meli['settingsstock'] = (float)sanitize_text_field($_POST['settingsstock']);
    if(isset($_POST['suffixprod'])) $this->config_sa2_meli['suffixprod'] = sanitize_text_field($_POST['suffixprod']);
    if(isset($_POST['prodbrand']) && !empty($_POST['prodbrand'])) $this->config_sa2_meli['prodbrand'] = sanitize_text_field($_POST['prodbrand']);
    else $this->config_sa2_meli['prodbrand'] = 'Marca Dos Produtos';
    if(isset($_POST['multprice'])) $this->config_sa2_meli['multprice'] = (float)sanitize_text_field($_POST['multprice']);
    if(isset($_POST['questiontime'])) $this->config_sa2_meli['questiontime'] = (int)sanitize_text_field($_POST['questiontime']);
    if(isset($_POST['syncimages'])) $this->config_sa2_meli['syncimages'] = (int)sanitize_text_field($_POST['syncimages']);
    if(isset($_POST['configmail'])) $this->config_sa2_meli['configmail'] = (bool)sanitize_text_field($_POST['configmail']);
    if(isset($_POST['prodquestion'])) $this->config_sa2_meli['prodquestion'] = (bool)sanitize_text_field($_POST['prodquestion']);
    if(isset($_POST['autoprodquestionfrete'])) $this->config_sa2_meli['autoprodquestionfrete'] = (bool)sanitize_text_field($_POST['autoprodquestionfrete']);
    if(isset($_POST['emailproductquestion']) && !empty($_POST['emailproductquestion'])) $this->config_sa2_meli['emailproductquestion'] = sanitize_text_field($_POST['emailproductquestion']);
    else $this->config_sa2_meli['emailproductquestion'] = 'example@mail.com,example@mail.com';
    if(isset($_POST['emailaftersale']) && !empty($_POST['emailaftersale'])) $this->config_sa2_meli['emailaftersale'] = sanitize_text_field($_POST['emailaftersale']);
    else $this->config_sa2_meli['emailaftersale'] = 'example@mail.com,example@mail.com';
    if(isset($_POST['emailnewsale']) && !empty($_POST['emailnewsale'])) $this->config_sa2_meli['emailnewsale'] = sanitize_text_field($_POST['emailnewsale']);
    else $this->config_sa2_meli['emailnewsale'] = 'example@mail.com,example@mail.com';
    if(isset($_POST['emailerror']) && !empty($_POST['emailerror'])) $this->config_sa2_meli['emailerror'] = sanitize_text_field($_POST['emailerror']);
    else $this->config_sa2_meli['emailerror'] = 'example@mail.com,example@mail.com';
    if (isset($_POST['syncprod'])) {
      $this->config_sa2_meli['syncprod'] = (bool)sanitize_text_field($_POST['syncprod']);
      $this->config_sa2_meli['attprodtitle'] = (bool)sanitize_text_field($_POST['attprodtitle']);
      $this->config_sa2_meli['attprodprice'] = (bool)sanitize_text_field($_POST['attprodprice']);
      $this->config_sa2_meli['attproddesctiption'] = (bool)sanitize_text_field($_POST['attproddesctiption']);
      $this->config_sa2_meli['attprodstock'] = (bool)sanitize_text_field($_POST['attprodstock']);
      $this->config_sa2_meli['attprodimages'] = (bool)sanitize_text_field($_POST['attprodimages']);
    }

    if(isset($_POST['from_mail']) && !empty($_POST['from_mail'])) $this->config_sa2_meli['from_mail'] = sanitize_text_field($_POST['from_mail']);
    else $this->config_sa2_meli['from_mail'] = 'example@mail.com,example@mail.com';
    if(isset($_POST['from_title']) && !empty($_POST['from_title'])) $this->config_sa2_meli['from_title'] = sanitize_text_field($_POST['from_title']);
    else $this->config_sa2_meli['from_title'] = 'WcMeli Sincronizador Automático';

    if(isset($_POST['syncorder'])) $this->config_sa2_meli['syncorder'] = (bool)sanitize_text_field($_POST['syncorder']);
    if(isset($_POST['smtp'])) {
      $this->config_sa2_meli['smtp'] = (bool)sanitize_text_field($_POST['smtp']);
      if(!empty($_POST['host'])) $this->config_sa2_meli['host'] = sanitize_text_field($_POST['host']);
      else $this->config_sa2_meli['host'] = 'host.host.host';
      $this->config_sa2_meli['auth'] = (bool)sanitize_text_field($_POST['auth']);
      if(!empty($_POST['username'])) $this->config_sa2_meli['username'] = sanitize_text_field($_POST['username']);
      else $this->config_sa2_meli['username'] = 'username';
      if(!empty($_POST['password'])) $this->config_sa2_meli['password'] = sanitize_text_field($_POST['password']);
      else $this->config_sa2_meli['password'] = 'password';
      if(!empty($_POST['secure'])) $this->config_sa2_meli['secure'] = sanitize_text_field($_POST['secure']);
      else $this->config_sa2_meli['secure'] = 'tls';
    }
    if(isset($_POST['token'])) $this->config_sa2_meli['token'] = sanitize_text_field($_POST['token']);
    // if(isset($_POST['userid'])) $this->config_sa2_meli['userid'] = sanitize_text_field($_POST['userid']);
  }

  /*
  * Get config_sa2_meli option from Db
  */
  public function wcsa2meliGetOptions() {
    $option = get_option(sanitize_text_field($this->option_column));
    if(!$option) return false;

    return (array)unserialize($option);
  }

  /*
  * If $_GET['action'] == 'update' we are saving settings sent from a settings page
  */
  public function wcsa2meliUpdateOptions($urlPage){

    $option = serialize($this->config_sa2_meli);

    $updateOption = update_option($this->option_column,$option);

    if($updateOption) {

      if(strpos($urlPage,'?') === false) $location = $urlPage.'?settings-updated' ;
      else $location = $urlPage.'&settings-updated' ;
      header( "Location: $location", true, 302 );
    } else {
      $location = $urlPage ;
      header( "Location: $location", true, 100 );
    }
  }

  public function wcsa2meliUpdateOption($urlPage = null, $index, $value) {
    global $is_IIS;

    $this->config_sa2_meli[$index] = $value;
    $option = serialize($this->config_sa2_meli);


    $updateOption = update_option($this->option_column,$option);

    if(is_null($urlPage)) return $updateOption;
    if($updateOption) {
      if(strpos($urlPage,'?') === false) $location = $urlPage.'?settings-updated' ;
      else $location = $urlPage.'&settings-updated' ;
        header( "Location: $location", true, 302 );
    } else {
      $location = $urlPage ;
        header( "Location: $location", true, 302 );
    }

  }
}

?>
