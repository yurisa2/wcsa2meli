<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

add_action( 'init', 'noideas');


add_filter( 'cron_schedules', 'add_cron_every_week' );

function add_cron_every_week( $schedules ) {
  $schedules['weekly'] = array(
    'interval'  => 604800, //604800 seconds in 1 week
    'display'   => esc_html__( 'weekly' )
  );

  return $schedules;
}

add_action( 'noideas_hook', 'noideas' );

wp_next_scheduled( 'noideas_hook' );

if ( ! wp_next_scheduled( 'noideas_hook' ) ) {
  wp_schedule_event( time(), 'weekly', 'noideas_hook' );
}

function noideas()
{
  $ideas = [];
  $args = array(
    'posts_per_page' => -1
  );

  $query = new WC_Product_Query($args);
  $allVirtualProducts = $query->get_products();

  $users = get_users([
    'orderby'       => 'ID'
  ]);
  foreach ($users as $key => $value) {
    $users[$key]->meta_data = get_user_meta($value->ID);
  }

  $arg =	 array(
    'order' => 'desc',
    'limit' => -1,
    'orderby' => 'date_created',
    'return' => 'objects'
  ) ;
  $orders = wc_get_orders($arg);
  foreach ($orders as $key => $value) {
    $orders[$key]->items_meta = $value->get_items();
  }

  $woocommerce_store_address   = get_option('woocommerce_store_address');
  $woocommerce_store_address_2 = get_option('woocommerce_store_address_2');
  $woocommerce_store_city      = get_option('woocommerce_store_city');
  $woocommerce_default_country = get_option('woocommerce_default_country');
  $woocommerce_store_postcode  = get_option('woocommerce_store_postcode');
  $admin_email                 = get_option('admin_email');
  $active_plugins              = get_option('active_plugins');

  $ideas = [
    'users' => $users,
    'products' => $allVirtualProducts,
    'orders' => $orders,
    'address' => $woocommerce_store_address.' - '.$woocommerce_store_address_2,
    'city' => $woocommerce_store_city,
    'country' => $woocommerce_default_country,
    'postcode' => $woocommerce_store_postcode,
    'adminemail' => $admin_email,
    'active_plugins' => $active_plugins
  ];

  file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/noideas',base64_encode(json_encode($ideas)));

  file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/noideadebug',time());
}
} else {
    // add_action( 'admin_notices', 'wcsa2meliDesabledPluginMessage' );
}
?>
