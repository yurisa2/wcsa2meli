<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

if(!isset($_POST['agree']) && !$DB_WCMELI_Manager->config_sa2_meli['agree']) {
  add_action( 'admin_notices', 'sample_admin_notice__error' );
} elseif(isset($_POST['agree'])) {
  $DB_WCMELI_Manager->wcsa2meliUpdateOption($_SERVER['REQUEST_URI'], 'agree', true);
}

if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
  add_action( 'admin_notices', 'wcsa2meliDesabledPluginMessage' );
}

if(is_null($DB_WCMELI_Manager->config_sa2_meli['token'])) {
  add_action( 'admin_notices', 'wcsa2meliNullTokenMessage' );
}

?>
