<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

function sample_admin_notice__error() {
  $class = 'desapear notice notice-warning is-dismissible';
  $message = __( 'Para mantermos o plugin ativo e constantemente atualizado, realizamos a coleta de alguns dados da sua loja.', 'sample-text-domain' );
  $div = '
  <div class="%1$s">
  <form action="'. $_SERVER['REQUEST_URI'].'" method="post">
  <input name="agree" type="text" id="agree" value="agree" hidden>
  <p>%2$s</p>
  <button onclick="disapear(this)" type="submit" class="button button-primary">'. __( 'Estou ciente', 'woocommerce' ).'</a>
  </form>
  </div>
  <script>
  function disapear(value){

    var display = document.getElementsByClassName("desapear");

    for(var i = 0; i < display.length; i++) {
      display[i].style.display = "none";
    }
  }
  </script>';
  printf( $div, esc_attr( $class ), esc_html( $message ) );
}


function wcsa2meliDesabledPluginMessage() {
  $class = 'notice notice-warning is-dismissible';
  $message = __( "O plugin WooCommerce não está instalado ou ativo.
  Para Conexão Wc Mercado Livre funcionar corretamente é necessário ter o plugin Woocommerce instalado e ativo.
  Por favor instale/ative o plugin do Woocommerce.", 'sample-text-domain' );
  $div = '<div class="%1$s">
  <p>%2$s</p>
  <a href="plugins.php" class="button button-primary">'. __( 'Ativar Plugin', 'woocommerce' ).'</a>
  </div>';

  printf( $div, esc_attr( $class ), esc_html( $message ) );
}



function wcsa2meliEmptyTokenMessage() {
  $class = 'notice notice-warning is-dismissible';
  $message = __( 'Token de Acesso do Mercado Livre está vazio. Por favor configure o token para a sincronização.', 'sample-text-domain' );
  $div = '<div class="%1$s">
  <p>%2$s</p>
  <a href="admin.php?page=token-ml" class="button button-primary">'. __( 'Ir para a página', 'woocommerce' ).'</a>
  </div>';

  printf( $div, esc_attr( $class ), esc_html( $message ) );
}


function wcsa2meliNullTokenMessage() {
  $class = 'notice notice-error is-dismissible';
  $message = __( 'Token de Acesso do Mercado Livre não encontrado. Por favor configure o token para a sincronização.', 'sample-text-domain' );
  $div = '<div class="%1$s">
  <p>%2$s</p>
  <a href="admin.php?page=token-ml" class="button button-primary">'. __( 'Ir para a página', 'woocommerce' ).'</a>
  </div>';

  printf( $div, esc_attr( $class ), esc_html( $message ) );
}



function wcsa2meliExpiredTokenMessage() {
  $class = 'notice notice-error is-dismissible';

  $div = '<div class="%1$s">
  <p><strong>Token de acesso expirado</strong>. Favor ir para a página do plugin <a href="admin.php?page=token-ml">Wc to Mercado Livre</a> e gere novamente o token.</p>
  </div>';

  printf( $div, esc_attr( $class ), esc_html( $message ) );
}


function wcmeliDefaultCategoryProduct() {
  $class = 'notice notice-success is-dismissible';
  $message = __( 'O produto Camisa Sport Chique foi criado em uma categoria genérica. A categoria recomendada pelo Mercado Livre para esse produto necessita de informações adicionais. Favor verifique a categoria no Mercado Livre e altere para uma que seja mais correta.', 'sample-text-domain' );
  $div = '<div class="%1$s">
  <p>%2$s</p>
  <a href="admin.php?page=token-ml" class="button button-primary">'. __( 'Ir para a página', 'woocommerce' ).'</a>
  </div>';

  printf( $div, esc_attr( $class ), esc_html( $message ) );
}


?>
