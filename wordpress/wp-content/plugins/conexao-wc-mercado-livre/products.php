<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


function wcsa2meliGetProductImageSrc($productId)
{
  if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
  $product = new WC_Product($productId);

  $attachmentIds = $product->get_gallery_attachment_ids();
$imgUrls = array();
foreach( $attachmentIds as $attachmentId )
{
  $imgUrls[] = wp_get_attachment_url( $attachmentId );
}

return $imgUrls;
} else {
  // add_action( 'admin_notices', 'wcsa2meliDesabledPluginMessage' );
}

}

 ?>
