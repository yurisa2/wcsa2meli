=== Conexão Wc Mercado Livre ===
Contributors: luigimuzy, yurisa2
Donate link:
Tags: mercado livre, ml, sincronização, conexão, sync, wc ml, automatico, atualizar produto.
Requires at least: 5.0
Tested up to: 5.3.2
Stable tag: 5.2.4
Requires PHP: 5.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Conexão Wc Mercado Livre permite a sincronização automática dos produtos cadastrados no Mercado Livre com os produtos cadastrados no Woocommerce através do vínculo do SKU do produto.

Conexão Wc Mercado Livre foi desenvolvido para sincronizar produtos cadastrados como simples no Woocomerce com produtos categorizados como novos e com o tipo de anúncio Clássico ou Premium no Mercado Livre devido a restição de atualização de preço que existe no anúncio do tipo gratuito.

== Installation ==

1. Carregue os arquivos do plug-in no diretório `/ wp-content / plugins / conexão-wc-mercado-livre` ou instale o plug-in diretamente na tela de plug-ins do WordPress.
2. Ative o plugin através da tela 'Plugins' no WordPress
3. Use a tela Configurações-> Nome do plug-in para configurar o plug-in
4. Abaixo estão as instruções e informações sobre como o plug-in funciona em detalhes

A primeira etapa é ter cadastrado os produtos na sua loja, sempre adicionando o SKU (encontrado no bloco Dados do Produto -> Inventário -> REF presente ao cadastrar ou modificar um produto) que será utilizado para vincular os produtos da loja Woocomerce com o Mercado Livre.

Feito o cadastro na loja, a segunda etapa é criar os produtos no Mercado Livre, sempre preenchendo o SKU correspondente ao mesmo produto  na sua loja.

Criado os produtos tanto na loja quanto no Mercado Livre e vinculando-os através do SKU, entramos nas configurações dentro do plugin.

Para tal, foi criado um icone no menu da esquerda no painel do admin da sua loja do woocomerce chamado Wc to Mercado Livre.

Deixe o mouse sobre esta opção e selecione "Conexão com Mercado Livre".

Para se conectar e fazer esta sincronização automática é necessário que você dê permissão para a nossa aplicação, bastando estar logado na conta que deseja sincronizar e clicar no botão "PEGAR TOKEN". Quando for pedido a autorização, confirme e será direcionado automaticamente a página onde irá conter uma caixa de texto com um monte de letras e números. Copie e cole este código no campo "Token (ACCESS TOKEN)" na tela do plugin e precione confirmar.

Agora só falta personalizar o plugin em Wc to Mercado Livre -> Configurações Avançadas. Personalize adições ao preço, diminuição do estoque, marca, tempo dos emails reportando erros e as pessoas que receberão estes emails.

== Frequently Asked Questions ==

= Is the use of sku mandatory? =

Yes. It is very important that the products have sku in both the woocommerce and Mercado Livre

= Is only simple product synchronization possible? What about products with variation or grouping? =

For the free version only simple products, being updated only price, stock and title.
Depending on the version of the plugin, we add other product types and more features to the plugin, such as full product synchronization, image updating and an ad questions tracking panel, with email triggering whenever you have new questions, plus ordering, with after sales question tracking panel.

== Screenshots ==

1. New menu icon created
2. Configurações Avançadas - Plugin configuration accessed by menu icon created
3. Conexão com Mercado Livre 1º Acesso - Plugin configuration accessed by menu icon created
4. Conexão com Mercado Livre ao preencher o token - Plugin configuration accessed by menu icon created
5. Versões existentes do plugin e seus benefícios

== Changelog ==

= 1.3.2 =
* Arrumado botão "mostrar/esconder" da função Perguntas nos Anúncios que clicando em qualquer um dos botões das perguntas já respondidas, mostrava ou escondia todos os demais.

= 1.3.1 =
* Nova função adicionada "Perguntas nos anúncios" (Novo ícone no menu).

= 1.2.3 =
* Arrumado erro ao ativar o plugin (problemas internos).
* Arrumado erro ao ativar o plugin sem ter woocommerce ativo.
* Arrumado erro na página de produtos do woocommerce.
* Adição de Link para o produto conectado no mercado livre em nova aba.
* Exibição de mensagem quando o woocommerce não estiver ativo.
* Exibição de texto para telemetria.
* Botão "Eu aceito" funcionando corretamente.

= 1.2.2 =
* Arrumado alguns erros e mudança de logo

= 1.2.1 =
* Link para a visualização das versões do plugin

= 1.2 =
* Automatização da renovação do token de acesso

= 1.1 =
* Compatibilidade com WordPress 5.3.1

= 1.0 =
* Adição de troca de conta em Conexão com Mercado Livre.

== Upgrade Notice ==
= 1.3.2 =
Arrumado botão "mostrar/esconder" da função Perguntas nos Anúncios que clicando em qualquer um dos botões das perguntas já respondidas, mostrava ou escondia todos os demais.

= 1.3.1 =
Adicionada nova funcionalidade ao plugin "Perguntas nos Anúncios".

= 1.2.3 =
Correção de diversos problemas que faziam o plugin não funcionar. Corre para atualizar e ter a funcionalidade do plugin.

= 1.2.2 =
Alguns erros arrumados

= 1.2.1 =
Adicionado link para visualizar as diferenças entre as versões do plugin

= 1.2 =
Criada forma para atualizar o token de acesso (access_token) de forma automática, sem precisar se preocupar.

= 1.1 =
Testada Compatibilidade com a nova versão do WordPress.

= 1.0 =
Possibilidade de trocar a conta que está conectado.
