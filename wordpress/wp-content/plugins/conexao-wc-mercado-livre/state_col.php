<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
  function wcmeli_custom_query_vars_filter_new_collunm($vars) {
    $vars[] .= 'post_type';
    return $vars;
  }
  add_filter( 'query_vars', 'wcmeli_custom_query_vars_filter_new_collunm' );

  // an associative array containing the query var and its value

$getdata = urlencode(base64_encode(json_encode(array('f'=>get_site_url(),'t'=>time()))));

$wp_remote_get_param = array(
  'timeout'     => 10
);
$gasReturn = json_decode(wp_remote_retrieve_body(wp_remote_get('https://sa2.com.br/wcsa2meli/wcmeli.php?gas='.$getdata,$wp_remote_get_param)));

define("WCMELI_APP_ID",base64_decode($gasReturn[0]));
define("WCMELI_SECRET_KEY",base64_decode($gasReturn[1]));
include_once WCMELI_PLUGIN_PATH.'wcml_v2/include/all_include.php';
if(!class_exists('flux')) include WCMELI_PLUGIN_PATH.'wcml_v2/include/sa2_flux/include/flux.php';

add_filter( 'manage_edit-product_columns', 'wcmeli_show_product_order',15 );
function wcmeli_show_product_order($columns){
   $columns['sync'] = __( 'Sincronizado');
   return $columns;
}

add_action( 'manage_product_posts_custom_column', 'wcmeli_product_column_offercode', 10, 2 );

function wcmeli_product_column_offercode( $column, $postid ) {

  $meliProduct = new meliProduct;
  $flux = new flux('wcmeli_product');

    if ( $column == 'sync' ) {
      $productInfo = wc_get_product($postid);
      if(strlen($productInfo->get_sku()) < 1) $productSku = $postid;
      else $productSku = $productInfo->get_sku();

      if(!file_exists(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')) file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode([])));
      $wcxml = (array)json_decode($flux->fluxiCode(file_get_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')),true);
      $inArray = false;
      foreach ($wcxml as $key => $value) {
        if($value['wc_type'] == 'sku' && $productSku == $value['wc'] || $value['wc_type'] == 'id' && $postid == $value['wc']) {
          $inArray = true;
          $permalink = $meliProduct->meliGetProduct($value['mlb'])['body']->permalink;
          break;
        } else $inArray = false;
      }

      if($inArray) {
        $params = array('post_type' => 'product','page' => 'conectar','action' => 'desconectar','wcprod' => $postid);

        // var_dump($meliProduct->meliGetProduct($value));
        if(empty($permalink)) $permalink = '';
        echo "<a target='_blank' href='".$permalink."'><b style='color:green;'>Conectado</b></a><br>";
        echo "<a class='desconectprod' target='_blank' href='".$permalink."' style='font-size:10px;'><span class='dashicons dashicons-external'></span>Anúncio ML</a><br>";
        echo '<a class="desconectprod" href="'.add_query_arg($params, 'edit.php').'" style="font-size:10px;">(Desconectar)</a>';
      } else {
        $params = array('post_type' => 'product','page' => 'conectar','action' => 'conectar','wcprod' => $postid);
        echo "<b style='color:red;'>Não Conectado</b><br>";
        echo '<a href="'.add_query_arg($params, 'edit.php').'" style="font-size:10px;">(Conectar)</a>';
      }
    }
}
} else {
  // add_action( 'admin_notices', 'wcsa2meliDesabledPluginMessage' );
}

 ?>
