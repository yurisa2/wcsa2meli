<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

function WCMELI_loadCss() {
    wp_register_style('WCMELI_loadCss', plugins_url('css/style.css',__FILE__ ));
    wp_enqueue_style('WCMELI_loadCss');
  }

add_action( 'admin_init','WCMELI_loadCss');

 ?>
