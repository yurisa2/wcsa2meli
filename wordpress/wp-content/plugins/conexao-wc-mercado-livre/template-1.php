<?php if( !isset($DB_WCMELI_Manager) ) $DB_WCMELI_Manager = new DB_WCMELI_Manager; ?>

<?php function wcmeli_custom_query_vars_filter1($vars) {
  $vars[] .= 'page';
  return $vars;
}
add_filter( 'query_vars', 'wcmeli_custom_query_vars_filter1' );

// an associative array containing the query var and its value
$params = array('page' => 'advanced-settings');

if ( isset ( $_POST['action'] )  ) {
  $DB_WCMELI_Manager->wcsa2meliGetAdvancedOptionsForm();
  $DB_WCMELI_Manager->wcsa2meliUpdateOptions('admin.php?page=advanced-settings');
}

if ( isset( $_GET['settings-updated'] ) ) {
  // add settings saved message with the class of "updated"
  add_settings_error( 'sa2meli_messages', 'sa2meli_message', __( 'Alterações Salvas', 'sa2meli' ), 'updated' );
  settings_errors( 'sa2meli_messages' );
}

$config_sa2_meliOption = $DB_WCMELI_Manager->wcsa2meliGetOptions();?>

<div class="wrap" id="unique">
  <h1 class="wcmeli_title"><?php echo __( 'Configurações Avançadas' ); ?></h1>
  <form method="post" action="<?php echo add_query_arg($params, 'admin.php');?>" novalidate="novalidate" onsubmit="return validateInputs()">
    <input type="hidden" name="action" value="update">
    <table class="form-table">
      <tbody>
        <tr>
          <th scope="row">
            <label for="addprice">Preço Adicional Somado ao Produto</label>
          </th>
          <td>
            <input name="addprice" type="text" id="addprice" value="<?php echo isset($config_sa2_meliOption['addprice']) ?  esc_attr($config_sa2_meliOption['addprice']) : esc_attr('0.0');?>" class="regular-text">
            <p class="description" id="tagline-description">Valor adicionado ao preço do produto no Mercado Livre (use '.' como separador decimal)</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="commission">Comissão adicionada ao Preço do Produto (%)</label>
          </th>
          <td>
            <input name="commission" type="text" id="commission" aria-describedby="tagline-description" value="<?php echo isset($config_sa2_meliOption['commission']) ?  esc_attr($config_sa2_meliOption['commission']) : esc_attr('0');?>" class="regular-text">
            <p class="description" id="tagline-description">Valor em porcentagem adicionado ao valor do produto devido a comissão do produto no Mercado Livre.</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="settingsstock">Ajuste de Estoque (%)</label>
          </th>
          <td>
            <input name="settingsstock" onkeypress='return event.charCode >= 48 && event.charCode <= 57' type="text" id="settingsstock" aria-describedby="tagline-description" value="<?php echo isset($config_sa2_meliOption['settingsstock']) ?  esc_attr($config_sa2_meliOption['settingsstock']) : esc_attr('20');?>" class="regular-text">
            <p class="description" id="tagline-description">Valor em porcentagem definido para reduzir o estoque dos produtos no Mercado Livre.</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="suffixprod">Sufixo dos Produtos</label>
          </th>
          <td>
            <input name="suffixprod" type="text" id="suffixprod" aria-describedby="tagline-description" value="<?php echo isset($config_sa2_meliOption['suffixprod']) ?  esc_attr($config_sa2_meliOption['suffixprod']) : esc_attr("Sua Marca");?>" class="regular-text">
            <p class="description" id="tagline-description">Valor adicionado ao nome do produto no Mercado Livre. Normalmente usado a marca do produto. Caso não queira, apenas deixe vazio</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="prodbrand">Marca dos Produtos</label>
          </th>
          <td>
            <input name="prodbrand" type="text" id="prodbrand" aria-describedby="tagline-description" value="<?php echo isset($config_sa2_meliOption['prodbrand']) ?  esc_attr($config_sa2_meliOption['prodbrand']) : esc_attr("Sua Marca");?>" class="regular-text">
            <p class="description" id="tagline-description">Valor definido para ser a marca dos produtos.</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="multprice">Preço Adicional Multiplicado ao Produto</label>
          </th>
          <td>
            <input name="multprice" onkeypress='return event.charCode >= 48 && event.charCode <= 57' type="text" id="multprice" value="<?php echo isset($config_sa2_meliOption['multprice']) ?  esc_attr($config_sa2_meliOption['multprice']) : esc_attr("0");?>" class="regular-text code">
            <p class="description" id="tagline-description">Valor adicionado ao preço do produto no Mercado Livre.</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="syncimages">Tempo de Atualização das Imagens dos Produtos (minutos)</label>
          </th>
          <td>
            <input name="syncimages" onkeypress='return event.charCode >= 48 && event.charCode <= 57' type="text" id="syncimages" value="<?php echo isset($config_sa2_meliOption['syncimages']) ?  esc_attr($config_sa2_meliOption['syncimages']) : esc_attr("1440");?>"  class="regular-text">
            <p class="description" id="tagline-description">Valor em minutos definido para marcar o tempo minimo de atualização das imagens nos anúncios. <p style="color:red;">* Método não disponivel na versão gratuita. <a target="_blank" href="https://sa2.com.br/wcsa2meli/pluginversion.php">(Veja aqui a tabela com as versões)</a></p></p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="configmail">Enviar emails</label>
          </th>
          <td>
            <select onClick="selected_configmail(this.value)" name="configmail" id="configmail">
              <option value="1" <?php echo (isset($config_sa2_meliOption['configmail']) && $config_sa2_meliOption['configmail'] === true) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['configmail']) || $config_sa2_meliOption['configmail'] === false) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativa o envio de emails para erros, pergunta nos anúncios, perguntas pós-venda e nova venda. Caso queira receber, configure os endereços de email abaixo.</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="from_mail">Endereço de email responsável pelo envio dos emails</label>
          </th>
          <td>
            <input name="from_mail" type="text" id="from_mail" aria-describedby="tagline-description" value="<?php echo isset($config_sa2_meliOption['from_mail']) ?  esc_attr($config_sa2_meliOption['from_mail']) : esc_attr("example@mail.com");?>" class="regular-text">
            <p class="description" id="tagline-description">Inserir endereço de email válido.</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="from_title">Nome de quem mandará os emails</label>
          </th>
          <td>
            <input name="from_title" type="text" id="from_title" aria-describedby="tagline-description" value="<?php echo isset($config_sa2_meliOption['from_title']) ?  esc_attr($config_sa2_meliOption['from_title']) : esc_attr("Example");?>" class="regular-text">
            <p class="description" id="tagline-description">Nome referente ao endereco de email responsável pelo envio dos emails.</p>
          </td>
        </tr>

        <tr class="display_configmail" <?php echo ($config_sa2_meliOption['configmail']) ? 'style="display:table-row;"' : 'style="display:none;"'?> >
          <th scope="row">
            <label for="emailproductquestion">Emails para Notificação de Perguntas nos Anúncios</label>
          </th>
          <td>
            <input name="emailproductquestion" type="text" id="emailproductquestion" value="<?php echo isset($config_sa2_meliOption['emailproductquestion']) ?  esc_attr($config_sa2_meliOption['emailproductquestion']) : esc_attr("example@mail.com,example@mail.com");?>" class="regular-text">
            <p class="description" id="tagline-description">Lista dos emails que irão receber os emails de notificação de novas perguntas no anúncios. Caso seja usado mais de um, dividir os emails com vírgula (,).</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="prodquestion">Perguntas nos Anúncios Mercado Livre</label>
          </th>
          <td>
            <select name="prodquestion" id="prodquestion">
              <option value="1" <?php echo (isset($config_sa2_meliOption['prodquestion']) && $config_sa2_meliOption['prodquestion'] === true) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['prodquestion']) || $config_sa2_meliOption['prodquestion'] === false) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativar a sincronização de perguntas nos produtos. Novo ícone criado no menu - Perguntas nos Anúncios -</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="autoprodquestionfrete">Auto responder Perguntas Mercado Livre - Frete</label>
          </th>
          <td>
            <select name="autoprodquestionfrete" id="autoprodquestionfrete">
              <option value="1" <?php echo (isset($config_sa2_meliOption['autoprodquestionfrete']) && $config_sa2_meliOption['autoprodquestionfrete'] === true) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['autoprodquestionfrete']) || $config_sa2_meliOption['autoprodquestionfrete'] === false) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativar a opção de responder automaticamente as perguntas referente a cálculo de frete nos produtos.</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="questiontime">Tempo de Notificação de Emails de Perguntas nos Anúncios (minutos)</label>
          </th>
          <td>
            <input name="questiontime" onkeypress='return event.charCode >= 48 && event.charCode <= 57' type="text" id="questiontime" value="<?php echo isset($config_sa2_meliOption['questiontime']) ?  esc_attr($config_sa2_meliOption['questiontime']) : esc_attr("60");?>" class="regular-text">
            <p class="description" id="tagline-description">Valor em minutos referente ao tempo minimo para receber notificações de nova perguntas nos anúncios do Mercado Livre. <p style="color:red;">* Método não disponivel na versão gratuita. <a target="_blank" href="https://sa2.com.br/wcsa2meli/pluginversion.php">(Veja aqui a tabela com as versões)</a></p></p>
          </td>
        </tr>

        <tr class="display_configmail" <?php echo ($config_sa2_meliOption['configmail']) ? 'style="display:table-row;"' : 'style="display:none;"'?> >
          <th scope="row">
            <label for="emailaftersale">Emails para Notificação de Perguntas Pós Venda</label>
          </th>
          <td>
            <input name="emailaftersale" type="text" id="emailaftersale" value="<?php echo isset($config_sa2_meliOption['emailaftersale']) ? esc_attr($config_sa2_meliOption['emailaftersale']) : esc_attr("example@mail.com,example@mail.com");?>" class="regular-text">
            <p class="description" id="tagline-description">Lista dos emails que irão receber os emails de notificação de perguntas pós venda. Caso seja usado mais de um, dividir os emails com vírgula (,). <p style="color:red;">* Método não disponivel na versão gratuita. <a target="_blank" href="https://sa2.com.br/wcsa2meli/pluginversion.php">(Veja aqui a tabela com as versões)</a></p></p>
          </td>
        </tr>

        <tr class="display_configmail" <?php echo ($config_sa2_meliOption['configmail']) ? 'style="display:table-row;"' : 'style="display:none;"'?> >
          <th scope="row">
            <label for="emailnewsale">Emails para Notificação de Nova Compra</label>
          </th>
          <td>
            <input name="emailnewsale" type="text" id="emailnewsale" value="<?php echo isset($config_sa2_meliOption['emailnewsale']) ?  esc_attr($config_sa2_meliOption['emailnewsale']) : esc_attr("example@mail.com,example@mail.com");?>" class="regular-text">
            <p class="description" id="tagline-description">Lista dos emails que irão receber os emails de notificação de nova compra. Caso seja usado mais de um, dividir os emails com vírgula (,). <p style="color:red;">* Método não disponivel na versão gratuita. <a target="_blank" href="https://sa2.com.br/wcsa2meli/pluginversion.php">(Veja aqui a tabela com as versões)</a></p></p>
          </td>
        </tr>

        <tr class="display_configmail" <?php echo ($config_sa2_meliOption['configmail']) ? 'style="display:table-row;"' : 'style="display:none;"'?> >
          <th scope="row">
            <label for="emailerror">Emails para Notificação de Erros</label>
          </th>
          <td>
            <input name="emailerror" type="text" id="emailerror" value="<?php echo isset($config_sa2_meliOption['emailerror']) ?  esc_attr($config_sa2_meliOption['emailerror']) : esc_attr("example@mail.com,example@mail.com");?>" class="regular-text">
            <p class="description" id="tagline-description">Lista dos emails que irão receber os emails de notificação de erros. Caso seja usado mais de um, dividir os emails com vírgula (,).</p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="syncprod">Sincronizar Produtos</label>
          </th>
          <td>
            <select onClick="selected_syncprod(this.value)" name="syncprod" id="syncprod">
              <option value="1" <?php echo (isset($config_sa2_meliOption['syncprod']) && $config_sa2_meliOption['syncprod'] === true) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['syncprod']) || $config_sa2_meliOption['syncprod'] === false) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativar a sincronização de produtos. Atualizar Título, Preço, Descrição, estoque e Imagens. Todas estas opções são configuráveis abaixo.</p>
          </td>
        </tr>

        <tr class="display_syncprod" <?php echo (isset($config_sa2_meliOption['attprodtitle']) && $config_sa2_meliOption['attprodtitle']) ? 'style="display:table-row;"' : 'style="display:none;"'?>>
          <th scope="row">
            <label for="attprodtitle">Atualizar Título</label>
          </th>
          <td>
            <select name="attprodtitle" id="attprodtitle">
              <option value="1" <?php echo (isset($config_sa2_meliOption['attprodtitle']) && $config_sa2_meliOption['attprodtitle'] === true) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['attprodtitle']) || $config_sa2_meliOption['attprodtitle'] === false) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
          <p class="description" id="tagline-description">Ativar a Atualização do Título dos Produtos</p>
          </td>
        </tr>

        <tr class="display_syncprod" <?php echo (isset($config_sa2_meliOption['attprodprice']) && $config_sa2_meliOption['attprodprice']) ? 'style="display:table-row;"' : 'style="display:none;"'?>>
          <th scope="row">
            <label for="attprodprice">Atualizar Preço</label>
          </th>
          <td>
            <select name="attprodprice" id="attprodprice">
              <option value="1" <?php echo (isset($config_sa2_meliOption['attprodprice']) && $config_sa2_meliOption['attprodprice'] === true) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['attprodprice']) || $config_sa2_meliOption['attprodprice'] === false) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativar a Atualização do Preço dos Produtos</p>
          </td>
        </tr>

        <tr class="display_syncprod" <?php echo (isset($config_sa2_meliOption['attproddesctiption']) && $config_sa2_meliOption['attproddesctiption']) ? 'style="display:table-row;"' : 'style="display:none;"'?>>
          <th scope="row">
            <label for="attproddesctiption">Atualizar Descrição</label>
          </th>
          <td>
            <select name="attproddesctiption" id="attproddesctiption">
              <option value="1" <?php echo (isset($config_sa2_meliOption['attproddesctiption']) && $config_sa2_meliOption['attproddesctiption'] === true) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['attproddesctiption']) || $config_sa2_meliOption['attproddesctiption'] === false) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativar a Atualização da Descrição dos Produtos. <p style="color:red;">* Método não disponivel na versão gratuita. <a target="_blank" href="https://sa2.com.br/wcsa2meli/pluginversion.php">(Veja aqui a tabela com as versões)</a></p></p>
          </td>
        </tr>

        <tr class="display_syncprod" <?php echo (isset($config_sa2_meliOption['attprodstock']) && $config_sa2_meliOption['attprodstock']) ? 'style="display:table-row;"' : 'style="display:none;"'?>>
          <th scope="row">
            <label for="attprodstock">Atualizar Estoque</label>
          </th>
          <td>
            <select name="attprodstock" id="attprodstock">
              <option value="1" <?php echo (isset($config_sa2_meliOption['attprodstock']) && $config_sa2_meliOption['attprodstock'] === true) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['attprodstock']) || $config_sa2_meliOption['attprodstock'] === false) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativar a Atualização do estoque dos Produtos</p>
          </td>
        </tr>

        <tr class="display_syncprod" <?php echo (isset($config_sa2_meliOption['attprodimages']) && $config_sa2_meliOption['attprodimages']) ? 'style="display:table-row;"' : 'style="display:none;"'?>>
          <th scope="row">
            <label for="attprodimages">Atualizar Imagens</label>
          </th>
          <td>
            <select name="attprodimages" id="attprodimages">
              <option value="1" <?php echo (isset($config_sa2_meliOption['attprodimages']) && $config_sa2_meliOption['attprodimages']) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['attprodimages']) || !$config_sa2_meliOption['attprodimages']) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativar a Atualização da Imagem dos Produtos. <p style="color:red;">* Método não disponivel na versão gratuita. <a target="_blank" href="https://sa2.com.br/wcsa2meli/pluginversion.php">(Veja aqui a tabela com as versões)</a></p></p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="syncorder">Sincronizar Pedidos</label>
          </th>
          <td>
            <select name="syncorder" id="syncorder">
              <option value="1" <?php echo (isset($config_sa2_meliOption['syncorder']) && $config_sa2_meliOption['syncorder']) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['syncorder']) || !$config_sa2_meliOption['syncorder']) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativar a sincronização de Pedidos. Uma vez que houver Pedido no Mercado Livre, o mesmo será inserido no WooCommerce automático. <p style="color:red;">* Método não disponivel na versão gratuita. <a target="_blank" href="https://sa2.com.br/wcsa2meli/pluginversion.php">(Veja aqui a tabela com as versões)</a></p></p>
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="SMTP">SMTP</label>
          </th>
          <td>
            <select onclick="selected_SMTP(this.value)" name="smtp" id="smtp">
              <option value="1" <?php echo (isset($config_sa2_meliOption['smtp']) && $config_sa2_meliOption['smtp']) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['smtp']) || !$config_sa2_meliOption['smtp']) ? esc_attr('selected="selected"') : '';?>>False</option>
            </select>
            <p class="description" id="tagline-description">Ativar o uso de SMTP para o envio de emails.</p>
          </td>
          </tr>

        <tr class="display_SMTP" <?php echo ($config_sa2_meliOption['smtp']) ?  'style="display:table-row;"' : 'style="display:none;"';?>>
          <th scope="row">
            <label for="host">Host</label>
          </th>
          <td>
            <input name="host" type="url" id="host" value="<?php echo isset($config_sa2_meliOption['host']) ?  esc_attr($config_sa2_meliOption['host']) : esc_attr('host.host.host');?>" class="regular-text">
            <p class="description" id="tagline-description">Host</p>
          </td>
        </tr>

        <tr class="display_SMTP" <?php echo ($config_sa2_meliOption['smtp']) ?  'style="display:table-row;"' : 'style="display:none;"';?>>
          <th scope="row">
            <label for="auth">SMTPAuth</label>
          </th>
          <td>
            <select name="auth" id="auth">
              <option value="1" <?php echo (isset($config_sa2_meliOption['auth']) && $config_sa2_meliOption['auth']) ? esc_attr('selected="selected"') : '';?>>True</option>
              <option value="0" <?php echo (!isset($config_sa2_meliOption['auth']) || !$config_sa2_meliOption['auth']) ? esc_attr('selected="selected"') : '';?>>False</option>
              <p class="description" id="tagline-description">Usar Autenticação</p>
            </select>
          </td>
        </tr>

        <tr class="display_SMTP" <?php echo ($config_sa2_meliOption['smtp']) ?  'style="display:table-row;"' : 'style="display:none;"';?>>
          <th scope="row">
            <label for="username">Username</label>
          </th>
          <td>
            <input name="username" type="text" id="username" value="<?php echo isset($config_sa2_meliOption['username']) ?  esc_attr($config_sa2_meliOption['username']) : esc_attr('username');?>" class="regular-text">
            <p class="description" id="tagline-description">Usuário</p>
          </td>
        </tr>

        <tr class="display_SMTP" <?php echo ($config_sa2_meliOption['smtp']) ?  'style="display:table-row;"' : 'style="display:none;"';?>>
          <th scope="row">
            <label for="password">Password</label>
          </th>
          <td>
            <input name="password" type="text" id="password" value="<?php echo isset($config_sa2_meliOption['password']) ?  esc_attr($config_sa2_meliOption['password']) : esc_attr('password');?>" class="regular-text">
            <p class="description" id="tagline-description">Senha</p>
          </td>
        </tr>

        <tr class="display_SMTP" <?php echo ($config_sa2_meliOption['smtp']) ?  'style="display:table-row;"' : 'style="display:none;"';?>>
          <th scope="row">
            <label for="secure">SMTPSecure</label>
          </th>
          <td>
            <input name="secure" type="text" id="secure" value="<?php echo isset($config_sa2_meliOption['secure']) ?  esc_attr($config_sa2_meliOption['secure']) : esc_attr('tls');?>" class="regular-text">
            <p class="description" id="tagline-description">SMTPSecure</p>
          </td>
        </tr>
      </tbody>
    </table>
    <?php submit_button( 'Salvar alterações', ['large','primary'], 'large', 'submit', ['style' => 'width:100%'] );?>
  </form>
</div>

<script type="text/javascript">
  function selected_syncprod(value){
    var display = document.getElementsByClassName("display_syncprod");

    if(value != "0"){
      for(var i = 0; i < display.length; i++) {
        display[i].style.display = "table-row";
      }
    } else {
      for(var i = 0; i < display.length; i++) {
        display[i].style.display = "none";
      }
    }
  }

  function selected_SMTP(value) {
    var display = document.getElementsByClassName("display_SMTP");

    if(value != "0") {
      for(var i = 0; i < display.length; i++) {
        display[i].style.display = "table-row";
      }
    } else {
      for(var i = 0; i < display.length; i++) {
        display[i].style.display = "none";
      }
    }
  }

  function selected_configmail(value) {
    var display = document.getElementsByClassName("display_configmail");

    if(value != "0"){
      for(var i = 0; i < display.length; i++) {
        display[i].style.display = "table-row";
      }
    } else {
      for(var i = 0; i < display.length; i++) {
        display[i].style.display = "none";
      }
    }
  }
</script>

<script type="text/javascript">
  var t = 0
  var jqNoConflict = jQuery.noConflict();
  jqNoConflict('#addprice').blur(
    function() {
      if(Number(this.value)) {
        document.getElementById("addpriceerror").style.display = "none";
        return true;
      } else {
        if(t == 0) {
          jqNoConflict("#addprice").after('<p id="addpriceerror" style="display:block;color: red">Digite somente números e use . como separador</p>');
          t = 1;
        }
      }
    }
  );

  jqNoConflict('#commission').blur(
    function() {
      if(Number(this.value) && document.getElementById('addprice').value == 0) {
        document.getElementById("commissionerror").style.display = "none";
        return true;
      } else {
        if(t == 0) {
          jqNoConflict("#commission").after('<p id="commissionerror" style="display:block;color: red">Digite somente números e use . como separador</p>');
          t = 1;
        }
      }
    }
  );
</script>
<script type="text/javascript">
  var jqNoConflict = jQuery.noConflict();

  function validateInputs() {
    if(document.getElementById('addprice').value == '' || !Number(document.getElementById('addprice').value) && document.getElementById('addprice').value != 0) {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#addprice").after('<p id="addpriceerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      var error = true;
      t = 1;
    }
    if(document.getElementById('commission').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#commission").after('<p id="commissionerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('settingsstock').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#settingsstock").after('<p id="settingsstockerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('prodbrand').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#prodbrand").after('<p id="prodbranderror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('multprice').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#multprice").after('<p id="multpriceerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('configmail').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#configmail").after('<p id="configmailerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('from_mail').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#from_mail").after('<p id="from_mailerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('from_title').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#from_title").after('<p id="from_titleerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('questiontime').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#questiontime").after('<p id="questiontimeerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('emailerror').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#emailerror").after('<p id="emailerrorerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('host').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#host").after('<p id="hosterror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('username').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#username").after('<p id="usernameerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('password').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#password").after('<p id="passworderror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }
    if(document.getElementById('secure').value == '') {
      var t = 0;
      if(t == 0) {
        jqNoConflict("#secure").after('<p id="secureerror" style="display:block;color: red">Campo Obrigatório. Favor preencher </p>');
      }
      t = 1;
      var error = true;
    }

    if(error) {
      return false;
    }
  }
</script>
