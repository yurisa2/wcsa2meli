<?php
$url = 'https://sa2.com.br/wcsa2meli/wcmeli.php';
$wcmeli_plugin_dir = plugin_dir_path(__FILE__);
$getdata = urlencode(base64_encode(json_encode(array('f'=>get_site_url(),'t'=>time()))));

$return = wp_remote_retrieve_body(wp_remote_get(esc_url($url).'?gau='.$getdata));

?>
<?php if ( isset( $_GET['settings-updated'] ) ) {
  add_settings_error( 'sa2meli_messages', 'sa2meli_message', __( 'Alterações Salvas', 'sa2meli' ), 'updated' );
  settings_errors( 'sa2meli_messages' );
}
if( !isset($DB_WCMELI_Manager) )  $DB_WCMELI_Manager = new DB_WCMELI_Manager;
$config_sa2_meliOption = $DB_WCMELI_Manager->wcsa2meliGetOptions();

if(isset($_REQUEST['q'])) {
  $DB_WCMELI_Manager->wcsa2meliUpdateOption('admin.php?page=token-ml', 'token', $_REQUEST['q']);
}

if(isset($_REQUEST['d'])) {
  $DB_WCMELI_Manager->wcsa2meliUpdateOption('admin.php?page=token-ml', 'token', null);
  if(file_exists($wcmeli_plugin_dir.'wcml_v2/include/files/ai_sk')) unlink($wcmeli_plugin_dir.'wcml_v2/include/files/ai_sk');

  $getdata = urlencode(base64_encode(json_encode(array('f'=>get_site_url(),'t'=>time()))));
  $return = json_decode(wp_remote_retrieve_body(wp_remote_get(esc_url($url).'?gas='.$getdata)));

  file_put_contents($wcmeli_plugin_dir.'wcml_v2/include/files/ai_sk',json_encode(array('time'=>time(),'a'=>$return[0],'b'=>$return[1])));
  $content = (array)json_decode(file_get_contents($wcmeli_plugin_dir.'wcml_v2/include/files/ai_sk'),true);

  echo true;
}
?>

<div class="wrap" id="unique">
  <h1 class="wcmeli_title"><?php echo __( 'Conexão com Mercado Livre' ); ?></h1>

  <form method="post" action="" novalidate="novalidate">

    <table class="form-table">

      <tbody>
        <tr>
          <th scope="row"><label for="hash">Token (ACCESS TOKEN)</label></th>
          <td><textarea class="hash" name="hash" type="text" id="hash" style="width:75%;height:150px;resize:none" value="" class="regular-text" <?php echo !empty($config_sa2_meliOption['token']) ? 'readonly="true"' : '';?>><?php echo !empty($config_sa2_meliOption['token']) ? $config_sa2_meliOption['token'] : '';?></textarea>
            <p class="description" id="tagline-description">Token de acesso da aplicação no Mercado Livre.</p></td>
          </tr>



          <?php if(!empty($config_sa2_meliOption['token'])) {
            $obj = json_decode(base64_decode($config_sa2_meliOption['token']));
            echo '<tbody><tr><th scope="row"><label for="hash">Access Token</label></th><td><input readonly="true" name="token" type="text" id="" value="'.$obj->access_token.'" class="regular-text"></td></tr>
            <tr><th scope="row"><label for="hash">Expires Time</label></th><td><input readonly="true" name="time" type="text" id="" value="'.$obj->expires_in.'" class="regular-text"></td></tr>
            <tr><th scope="row"><label for="hash">Refresh Token</label></th><td><input readonly="true" name="refresh" type="text" id="" value="'.$obj->refresh_token.'" class="regular-text"></td></tr>
            <tr><th scope="row"><label for="hash">Conta conectada</label></th><td><input readonly="true" name="refresh" type="text" id="" value="'.$obj->nickname.'" class="regular-text"></td></tr>
            <tr><th scope="row"><label for="hash">Nome</label></th><td><input readonly="true" name="refresh" type="text" id="" value="'.$obj->name.'" class="regular-text"></td></tr>
            <tr><th scope="row"><label for="hash">Email</label></th><td><input readonly="true" name="refresh" type="text" id="" value="'.$obj->email.'" class="regular-text"></td></tr>';
            if($obj->access_token != '' && $obj->expires_in != '' && $obj->refresh_token != '' && $obj->nickname != '' && $obj->name != '' && $obj->email != '') {

              echo '<tr><th scope="row"><input readonly="true" name="refresh" type="text" id="" value="DADOS VALIDADOS" class="regular-text"></th>
              <td><span class="dashicons-before dashicons-yes"></span></td></tr></tbody>';
            } else {
              echo '<tr><th scope="row"><input readonly="true" name="refresh" type="text" id="" value="DADOS VALIDADOS" class="regular-text"></th>
              <td><span class="dashicons-before dashicons-no"></span></td></tr></tbody>';
            }
          } else {
            ?>
            <tr>
              <th scope="row"></th>
              <td class="confirm"></td>
            </tr>

          </tbody>
          <tbody class="log">
          </tbody>
          <?php
        }
        ?>

      </table>
    </form>
    <br>
    <br>
    <br>
    <br>
    <label class="notclickable">Para trocar de conta, basta clicar no botão abaixo. Os dados da conta, como token de acesso e token de renovação, serão apagados. </label>
    <p class="submit">
      <button type="submit" style="width:100%;" onclick="changeAccount()" name="submit" id="submit" class="button button-primary" value="">Trocar de Conta</button>
    </p>
    <form method="post" action="<?php echo $return; ?>" novalidate="novalidate" target="_blank">
      <tbody>
        <tr>
          <td><label class="notclickable">Caso seja seu primeiro acesso ou tenha perdido o código, não se prepcupe. Basta clicar no botão abaixo e será redirecionado para obter seu código.<br>Caso seja seu primeiro acesso, lembre-se de estar logado na conta Mercado Livre que deseja ser sincronizada, além de confirmar o acesso deste plugin ao conteúdo da sua loja Mercado Livre. Não se preocupe, após clicar no botão abaixo, aparecerá a mensagem se dejesa autorizar o App Woocommerce MercadoLivre Sync a se conectar com o Mercado Livre </label>
          </tr>
          <?php submit_button( 'PEGAR TOKEN', ['large','primary'], 'large', 'submit', ['style' => 'width:100%'] ); ?>
        </form>

        <script type="text/javascript">
        var jqNoConflict = jQuery.noConflict();
        var t = 0;
        jqNoConflict("textarea").blur(function(){
          if(window.t === 0) {
            str = atob(this.value);
            obj = JSON.parse(str);

            jqNoConflict('.log').append(jqNoConflict('<tbody/>').innerHTML ='<tr><th scope="row"><label for="hash">Access Token</label></th><td><input readonly="true" name="token" type="text" id="" value="'+obj.access_token+'" class="regular-text"></td></tr>');
            jqNoConflict('.log').append(jqNoConflict('<tbody/>').innerHTML ='<tr><th scope="row"><label for="hash">Expires Time</label></th><td><input readonly="true" name="time" type="text" id="" value="'+obj.expires_in+'" class="regular-text"></td></tr>');
            jqNoConflict('.log').append(jqNoConflict('<tbody/>').innerHTML ='<tr><th scope="row"><label for="hash">Refresh Token</label></th><td><input readonly="true" name="refresh" type="text" id="" value="'+obj.refresh_token+'" class="regular-text"></td></tr>');
            jqNoConflict('.log').append(jqNoConflict('<tbody/>').innerHTML ='<tr><th scope="row"><label for="hash">Conta conectada</label></th><td><input readonly="true" name="refresh" type="text" id="" value="'+obj.nickname+'" class="regular-text"></td></tr>');
            jqNoConflict('.log').append(jqNoConflict('<tbody/>').innerHTML ='<tr><th scope="row"><label for="hash">Nome</label></th><td><input readonly="true" name="refresh" type="text" id="" value="'+obj.name+'" class="regular-text"></td></tr>');
            jqNoConflict('.log').append(jqNoConflict('<tbody/>').innerHTML ='<tr><th scope="row"><label for="hash">Email</label></th><td><input readonly="true" name="refresh" type="text" id="" value="'+obj.email+'" class="regular-text"></td></tr>');
            jqNoConflict('.log').append(jqNoConflict('<tbody/>').innerHTML ='<tr><th scope="row"><input readonly="true" name="refresh" type="text" id="" value="DADOS VALIDADOS" class="regular-text"></th>');
            if(obj.access_token != '' && obj.expires_in != '' && obj.refresh_token != '' && obj.nickname != '' && obj.name != '' && obj.email != '') {
              jqNoConflict('.log').append(jqNoConflict('<th/>').innerHTML ='<td><span class="dashicons-before dashicons-yes"></span></td></tr>');
            }
            this.readOnly = true;
            document.getElementById("confirm").style.display = "none";
            window.t = 1;

}
            var oReq = new XMLHttpRequest();
            // oReq.onload = reqListener;
            var url = window.location.href;
            // var index = url.lastIndexOf("wp-admin");
            // url = url.substr(0,index);

            oReq.open("POST", url+"&q="+this.value, true);
            oReq.send();
        });

  </script>
    <script type="text/javascript">
    var jqNoConflict = jQuery.noConflict();
            var t2 = 0;
          function confirm() {
            var jqNoConflict = jQuery.noConflict();

            if(window.t2 === 0) {
              jqNoConflict('.confirm').append(jqNoConflict('<td/>').innerHTML ='<a id="confirm" onclick="Disapear()" class="confirm button button-primary">Confirmar</a>');
              window.t2 = 1;
            }
          }

          jqNoConflict('textarea').focus(function(){confirm();})

</script>
        <script type="text/javascript">
          function Disapear(){
            document.getElementById("confirm").style.display = "none";
          }


          function changeAccount() {
            var jqNoConflict = jQuery.noConflict();

            var url = window.location.href;

            jqNoConflict.ajax({
              url: url+"&d=true",
              type: 'DELETE',
              async: false,
              success: function(data) {

                location.reload(true); // then reload the page.(3)
              }
            });

          }
          </script>
