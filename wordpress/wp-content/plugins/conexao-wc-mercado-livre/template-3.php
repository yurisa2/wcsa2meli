<?php
function wcmeli_custom_query_vars_filter($vars) {
  $vars[] .= 'page';
  return $vars;
}
add_filter( 'query_vars', 'wcmeli_custom_query_vars_filter' );

// an associative array containing the query var and its value
$params = array('page' => 'auto-responder');

if(!class_exists('meliProduct')) require 'wcml_v2/include/all_include.php';
$wcmeliProduct = new meliProduct;
$fluxProductQuestion = new flux('wcmeli_product_question');

if ( isset ( $_POST['action'] )  ) :
  $resposta = $_POST["txtarea"];
  $id = $_POST["id"];
  if($resposta == '') :
    add_settings_error( 'sa2meliempty_messages', 'sa2meli_message', __( 'Resposta não pode estar vazia.', 'sa2meli' ) );
    settings_errors( 'sa2meliempty_messages' );
  else :
    $resposta = $wcmeliProduct->meliAnswerQuestion($id, $resposta);
    sleep(2);
    if($resposta['body']->status == 400) :
      add_settings_error( 'sa2melierror_messages', 'sa2meli_message', __( 'Não foi possivel responder a pergunta. Tente novamente.', 'sa2meli' ) );
      settings_errors( 'sa2melierror_messages' );
    else :
      $fluxProductQuestion->add_item($id);

      add_settings_error( 'sa2melisuccess_messages', 'sa2meli_message', __( 'Resposta enviada com sucesso', 'sa2meli' ), 'updated' );
      settings_errors( 'sa2melisuccess_messages' );
    endif;
  endif;
endif;

    $fluxProductQuestion->pathListItem = true;
    if(!$fluxProductQuestion->setFiles()) :
      $questionIds = $wcmeliProduct->meliGetQuestionIds();
      $fluxProductQuestion->list_item = $questionIds;
      $fluxProductQuestion->setFiles();
    endif;
    $fluxProductQuestion->getFiles();
    $productId = $fluxProductQuestion->next_item();
    if($productId != false) :
      $response = $wcmeliProduct->meliGetProduct($productId)['body'];
      $customerQuestions = $wcmeliProduct->get('questions/search',['item'=>$productId])['body'];
      sort($customerQuestions->questions); ?>
      <div class="wrap" id="unique">
        <h1 class="wcmeli_title"><?php echo __( 'Perguntas e Respostas - Mercado Livre'); ?></h1>
        <?php foreach ($customerQuestions->questions as $key => $value) :
          if($value->status == 'UNANSWERED') $unanswered[] = $value->id;
          else $answered[] = $value->id;
        endforeach;
        if(isset($unanswered)) :
          foreach ($unanswered as $key => $value) :
            $pergunta = $wcmeliProduct->meliGetQuestion($value);
            if(empty($pergunta['body'])) : ?>
                <div class="form-group erro">
                  Não foi possível se conectar com o MErcado Livre. Tente novamente.
                </div>
              </div>
              <?php exit;
            endif;
            $pergunta = $pergunta['body'];
            $param = array('access_token' => $wcmeliProduct->accessToken);
            $user = $wcmeliProduct->get('users/'.$pergunta->from->id,$param)['body'];?>
              <form method="post" action="<?php echo add_query_arg($params, 'admin.php');?>" novalidate="novalidate">
                <input type="hidden" name="action" value="answer">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" name="id" hidden="true" readonly value="<?php echo $pergunta->id; ?>"/>
              <?php if($key == 0) : ?>
                      <center><h2>PRODUTO <a target="_blank" href=<?php echo "$response->permalink";?>><?php echo $response->title; ?></a></h2></center>
                      <h2>PERGUNTAS NÃO RESPONDIDAS</h2>
                    <?php endif; ?>
                    <div class='unanswered'>
                      <div style="border:1px solid #0062CC;margin:10px;padding:10px;">
                        <h2>PERGUNTA</h2>
                        <textarea type="text" name="pergunta" class="form-control" readonly value=""><?php echo esc_html($pergunta->text."\n- ". $user->nickname .' - '. substr($pergunta->date_created, 0, 10).' '.substr($pergunta->date_created, 11, 8) );?></textarea>
                        <h2>RESPOSTA</h2>
                        <textarea name="txtarea" value="resposta" cols="75" rows="5"></textarea><br>
                        <div class="form-group">
                          <input type="submit" name="btnSubmit" class="btnContact" value="Responder" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
        <?php endforeach;
      else :
        unlink($fluxProductQuestion->pathListItem);
        echo '<meta http-equiv="refresh" content="1">';
      endif;
      if(isset($answered)) : ?>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <div class='answered'>
              <h2>PERGUNTAS RESPONDIDAS</h2>
              <?php foreach ($answered as $key => $value) :
                $pergunta = $wcmeliProduct->meliGetQuestion($value);
                if(empty($pergunta['body'])) : ?>
                <div class="wrap" id="unique">
                  <h1 class="wcmeli_title"><?php echo __( 'Perguntas e Respostas - Mercado Livre ( '.count($fluxProductQuestion->list_item).')'); ?></h1>
                  <div class="form-group erro">
                    Não foi possível se conectar com o MErcado Livre. Tente novamente.
                  </div>
                </div>
                <?php exit;
              endif;
              $pergunta = $pergunta['body'];

              $params = array('access_token' => $wcmeliProduct->accessToken);
              $user = $wcmeliProduct->get('users/'.$pergunta->from->id,$params)['body']; ?>

              <div style="border:1px solid #0062CC;margin:10px;padding:10px;">
                <div class="question">
                  <span class="wcmeli_title"><?php echo __($pergunta->text .' - '.$user->nickname.' - '.substr($pergunta->date_created, 0, 10).' '.substr($pergunta->date_created, 11, 8)); ?></span>
                  <button value="hidden-answer<?php echo $key;?>" class="toggle">Mostrar/Esconder Resposta</button>
                </div>
                <div class="hidden-answer<?php echo $key;?>" style="display:none">
                  <h3>Resposta</h3>
                  <textarea type="text" name="pergunta" class="form-control" readonly value=""><?php echo $pergunta->answer->text; ?></textarea>
                </div>
              </div>

            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>
  </div>
<?php else :
  ?>
  <div class="wrap" id="unique">
    <h1 class="wcmeli_title"><?php echo __( 'Perguntas e Respostas - Mercado Livre (0)'); ?></h1>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <div class="painel nonquestions">
            <h1 class="wcmeli_title title"><?php echo __( 'NÃO HÁ NOVAS PERGUNTAS'); ?></h1>
            <?php echo __( 'Você ficará sabendo através do envio de email.'); ?>
          </div>
        </div>
      </div>

    </div>
  </div><?php endif; ?>
<script>
var jqNoConflict = jQuery.noConflict();
jqNoConflict(document).ready(function(){
  jqNoConflict("button").click(function(){
    jqNoConflict('.'+this.value).toggle();
  });
});
</script>
