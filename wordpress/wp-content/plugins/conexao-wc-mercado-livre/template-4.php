<?php
  function wcmeli_custom_query_vars_template_wcimporter($vars) {
  $vars[] .= 'page';
  return $vars;
  }
  add_filter( 'query_vars', 'wcmeli_custom_query_vars_template_wcimporter' );

  // an associative array containing the query var and its value
  $params = array('page' => 'importer-mlproducts');
  if(!class_exists('flux')) include WCMELI_PLUGIN_PATH.'wcml_v2/include/sa2_flux/include/flux.php';

  $flux = new flux('wcmeli_product');

  if(!file_exists(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')) file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode([])));
  $wcxml = (array)json_decode($flux->fluxiCode(file_get_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')),true);

  if(!class_exists('meliProduct')) require 'wcml_v2/include/all_include.php';

  $wcmeliProduct = new meliProduct;
  // // $fluxProductQuestion = new flux('wcmeli_product');
  // // if(!$fluxProductQuestion->setFiles()) :

  // //   $fluxProductQuestion->list_item = $productIds;
  // //   $fluxProductQuestion->setFiles();
  // // endif;
  // // $fluxProductQuestion->getFiles();
  // // $productId = $fluxProductQuestion->next_item();

  if(isset($_POST['all'])) :
    include 'wcml_v2/includes/wcmeli_get.php';
    if(isset($return)) :
      $allProducts = $wcmeliProduct->meliGetProducts();


      $productAttributes['productList'] = $allProducts;
      $productAttributes['token'] = $wcmeliProduct->accessToken;
      $productAttributes['f'] = get_site_url();

      $productData = $wcmeliProduct->updatewcsa2meli(json_encode($productAttributes),base64_decode($return[0]),base64_decode($return[1]));

      $body = array(
          'gap' => $productData
      );

      $args = array(
          'body' => $body,
          'timeout' => '60',
          'redirection' => '5',
          'httpversion' => '1.0',
          'blocking' => true,
          'headers' => array(),
          'cookies' => array()
      );
      $url = 'https://sa2.com.br/wcsa2meli/wcmeli.php';
      $result = wp_remote_retrieve_body(wp_remote_post($url, $args));

      $return = json_decode($result);

      foreach ($return as $key => $value) :

        try {
          $product = new WC_Product;
          $product->set_name($value->title);
          $product->set_description($value->description);
          $product->set_regular_price($value->price);
          $product->set_manage_stock(true);
          $product->set_stock_quantity($value->available_quantity);
          if(strlen($wcmeliProduct->meliGetProductSku($value->id)) > 0) :
            $product->set_sku($wcmeliProduct->meliGetProductSku($value->id));//SKU-ML
            $sku_sku = true;
          else :
            $product->set_sku($value->id);//MLBXXXXXXXXx
            $sku_id = true;
          endif;
          $create = $product->save();
          if($create == 0) $notCreated[$value->id] = 'Invalid or duplicated SKU.';
          else $created[] = $value;

          if($sku_sku) :
            if($wcmeliProduct->meliGetProductSku($value->id)) :
              $wcxml[] = [
                'id' => end($wcxml)['id']+1,
                'wc' => $wcmeliProduct->meliGetProductSku($value->id),
                'wc_type' => 'sku',
                'mlb' => $value->id,
                'mlb_type' => 'id'
              ];
              file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode($wcxml)));
            endif;
          endif;
          if($sku_id) :
            $wcxml[] = [
              'id' => end($wcxml)['id']+1,
              'wc' => $create,
              'wc_type' => 'sku',
              'mlb' => $value->id,
              'mlb_type' => 'id'
            ];
            file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode($wcxml)));
          endif;
        } catch (Exception $ex) {
          /* ERROR LIKE "SKU ALREADY EXISTS" */
          $notCreated[$value->id] = $ex->getMessage();
        }
      endforeach;

      if( isset( $created ) ) :
        add_settings_error( 'sa2meliproduct_success', 'sa2meliproduct_success', __( count($created).' produtos foram criados com sucesso.' ),'success' );
        settings_errors( 'sa2meliproduct_success' );
      endif;
      if( isset( $notCreated ) ) :
        foreach ($notCreated as $key => $value) :
          add_settings_error( 'sa2meliproduct_error'.$key, 'sa2meliproduct_error', __( 'O produto '.$key.' não foi criado. '.$value ) );
          settings_errors( 'sa2meliproduct_error'.$key );
        endforeach;
      endif;
    endif;
  endif;
if(isset($_POST['select'])) :
  include 'wcml_v2/includes/wcmeli_get.php';
  if(isset($return)) :
    $productAttributes['productList'] = $_POST;
    $productAttributes['token'] = $wcmeliProduct->accessToken;
    $productAttributes['f'] = get_site_url();

    $productData = $wcmeliProduct->updatewcsa2meli(json_encode($productAttributes),base64_decode($return[0]),base64_decode($return[1]));

    $body = array(
    'gpl' => $productData
    );

    $args = array(
    'body' => $body,
    'timeout' => '60',
    'redirection' => '5',
    'httpversion' => '1.0',
    'blocking' => true,
    'headers' => array(),
    'cookies' => array()
    );
    $url = 'https://sa2.com.br/wcsa2meli/wcmeli.php';
    $result = wp_remote_retrieve_body(wp_remote_post($url, $args));

    // var_dump($result);
    // exit;
    $result = json_decode($result);
    foreach ($result as $key => $value) :
      try {
        $product = new WC_Product;
        if(!strpos($value->title,WCMELI_SUFFIX_PROD)) $product->set_name($value->title);
        else $product->set_name(str_replace(WCMELI_SUFFIX_PROD,'',$value->title));
        $product->set_description($value->description);

        $product->set_price((float)floatval($value->price));
        $product->set_regular_price((float)floatval($value->price));
        // var_dump($value->price);
        // exit;
        $product->set_manage_stock(true);
        $product->set_stock_quantity($value->available_quantity);
        if(strlen($wcmeliProduct->meliGetProductSku($value->id)) > 0) :
          $product->set_sku($wcmeliProduct->meliGetProductSku($value->id));//SKU-ML
          $sku_sku = true;
        else :
          $sku_id = true;
        endif;
        $create = $product->save();
        if($create == 0) :
          $notCreated[$value->id] = 'Invalid or duplicated SKU.';
        else :
          $created[] = $value->id;

          if(isset($sku_sku) && $sku_sku) :
            $wcxml[] = [
              'id' => end($wcxml)['id']+1,
              'wc' => $wcmeliProduct->meliGetProductSku($value->id),
              'wc_type' => 'sku',
              'mlb' => $value->id,
              'mlb_type' => 'id'
            ];
          endif;
          if(isset($sku_id) && $sku_id) :
            $wcxml[] = [
              'id' => end($wcxml)['id']+1,
              'wc' => $create,
              'wc_type' => 'id',
              'mlb' => $value->id,
              'mlb_type' => 'id'
            ];
          endif;
        endif;
      } catch (Exception $ex) {
        /* ERROR LIKE "SKU ALREADY EXISTS" */
        $notCreated[$value->id] = $ex->getMessage();
      }
    endforeach;
    file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode($wcxml)));
    if( isset( $created ) ) :
      add_settings_error( 'sa2meliproduct_success', 'sa2meliproduct_success', __( count($created).' produtos foram criados com sucesso.' ),'success' );
      settings_errors( 'sa2meliproduct_success' );
    endif;
    if( isset( $notCreated ) ) :
      foreach ($notCreated as $key => $value) :
        add_settings_error( 'sa2meliproduct_error'.$key, 'sa2meliproduct_error', __( 'O produto '.$key.' não foi criado. '.$value ) );
        settings_errors( 'sa2meliproduct_error'.$key );
      endforeach;
    endif;
  endif;
endif;
?>

  <?php $productIds = $wcmeliProduct->meliGetProducts(); ?>
  <div class="wrap" id="unique">
    <h1 class="wcmeli_title"><?php echo __( 'Importar Produtos do Mercado Livre para o Woocommerce'); ?></h1>
      <input type="hidden" name="action" value="answer">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <form method="post" action="<?php echo add_query_arg($params, 'admin.php');?>" novalidate="novalidate">
              <?php $runs = 0; ?>
              <?php foreach ($productIds as $key => $value) :
                $found = false;
                $mlProdSku = $wcmeliProduct->meliGetProductSku($value);
                if(count($wcxml) > 0) :
                  foreach( $wcxml as $_key => $_value ) :
                    if( $_value['mlb'] == $value ) $found = true;
                  endforeach;
                endif;

                if(!$mlProdSku) $existProdWithSku = false;
                else $existProdWithSku = wc_get_product_id_by_sku($mlProdSku);

                if( !wc_get_product_id_by_sku($value) && !$existProdWithSku && !$found) :
                  $haveProducts = true;
                  $productInfo = $wcmeliProduct->meliGetProduct($value);

                  $productDescription = $wcmeliProduct->meliGetProductDescription($value);
                    if( !is_null($productInfo['body']) ) : ?>
                      <?php if(count($wcmeliProduct->meliGetProduct($value)['body']->variations) < 1) : ?>
                      <?php if($runs == 0) : ?>
                        <div id="select-all-products">
                          <div style="border:1px solid #0062CC;margin:10px;padding:10px;">
                            <input type="checkbox" name="select-all" id="select-all" /><span class="wcmeli_title"><strong><?php echo __( 'Selecionar Todos' ); ?></strong></span>
                          </div>
                        </div>
                        <?php $runs++;?>
                      <?php endif; ?>
                        <div id="all-products">
                          <div style="border:1px solid #0062CC;margin:10px;padding:10px;">
                            <div class="product-<?php echo esc_attr($value); ?>">
                              <input type="checkbox" name="<?php echo __($value); ?>" value="<?php echo __($value); ?>"><span class="wcmeli_title"><?php echo __($productInfo['body']->title); ?></span>
                              <button type="button" value="hidden-prodinfo<?php echo $key;?>" class="button toggle">Mostrar/Esconder Resposta</button>
                            </div>
                            <div class="hidden-prodinfo<?php echo $key;?>" style="display:none">
                              <table class="wp-list-table widefat fixed striped posts">
                              	<tbody id="the-list">
                                  <tr>
                                    <th scope="row">
                                      <div id="gallery">
                                        <?php foreach ($productInfo["body"]->pictures as $key => $_picture) : ?>
                                          <?php if(count($productInfo["body"]->pictures) >= 4) : ?>
                                            <?php if($key == 0) : ?>
                                              <div id="main-picture" >
                                                <img src="<?php echo $_picture->url; ?>" width="224" height="224"/>
                                              </div>
                                              <div id="secondary-pictures">
                                            <?php endif; ?>
                                            <?php if($key > 0 && $key < 4) : ?>
                                              <img class="secondary-pictures" src="<?php echo $_picture->url; ?>" width="70" height="70"/>
                                            <?php endif; ?>
                                            <?php if($key+1 == count($productInfo["body"]->pictures)) : ?>
                                                </div>
                                            <?php endif; ?>
                                          <?php endif; ?>

                                          <?php if(count($productInfo["body"]->pictures) < 4) : ?>
                                            <?php if($key == 0) : ?>
                                              <div id="main-picture" >
                                                <img src="<?php echo $_picture->url; ?>" width="224" height="224"/>
                                              </div>
                                              <div id="secondary-pictures">
                                            <?php endif; ?>
                                            <?php if($key > 0 && $key < 4) : ?>
                                              <img class="secondary-pictures" src="<?php echo $_picture->url; ?>" width="70" height="70"/>
                                            <?php endif; ?>

                                            <?php if($key+1 == count($productInfo["body"]->pictures)) : ?>
                                              <?php for ($i=4; $i > count($productInfo["body"]->pictures); $i--) : ?>
                                                  <img src="https://sa2.com.br/wcsa2meli/produto-sem-imagem.jpg" class="secondary-pictures" src="" width="70" height="70"/>
                                              <?php endfor; ?>
                                                </div>
                                            <?php endif; ?>

                                          <?php endif; ?>
                                        <?php endforeach; ?>
                                      </div>
                                      <div id="price">
                                        <span class='wcmeli_title'>R$<?php echo number_format($productInfo["body"]->price,2, ',', ''); ?></span><br>
                                      </div>
                                      <div id="stock">
                                        <span class='wcmeli_title'>Estoque: <?php echo $productInfo["body"]->available_quantity; ?></span><br>
                                      </div>
                                    </th>
                                    <td>
                                      <div id="description">
                                        <span class='wcmeli_title'><?php echo $productDescription["body"]->plain_text; ?></span><br>
                                      </div>
                                      <div id="link">
                                        <span class='wcmeli_title'><a target="_blank" href="<?php echo $productInfo["body"]->permalink; ?>"><?php echo $productInfo["body"]->permalink; ?></a></span><br>
                                      </div>
                                    </td>

                                  </tr>
                              	</tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      <?php else : ?>
                        <div style="border:1px solid #0062CC;margin:10px;padding:10px;">
                          <div class="product-<?php echo esc_attr($value); ?>">
                            <span class="wcmeli_title"><?php echo __('Produto '.$productInfo['body']->title.' com variação'); ?></span>
                          </div>
                        </div>
                      <?php endif; ?>
                    <?php else : ?>

                    <?php endif; ?>
                  <?php endif;?>
              <?php endforeach; ?>
              <?php if(!isset($haveProducts)) : ?>
                <div id="all-products" class="painel nonquestions">
                  <h1 class="wcmeli_title title"><?php echo __( 'TODOS OS PRODUTOS FORAM CRIADOS'); ?></h1>
                </div>
              <?php else : ?>
              <div class="row submit">
                <input type="submit" name="select" id="select" class="button button-large button-primary" value="Importar Selecionados" style="width:100%">
                <!-- <input type="button" name="select-all" id="select-all" class="button button-large button-primary" value="Importar Todos" style="width:49%"> -->
              </div>
              <?php endif; ?>
            </form>
          </div>
        </div>
      </div>
  </div>
  <script>
  var jqNoConflict = jQuery.noConflict();
  jqNoConflict(document).ready(function(){
    jqNoConflict("button").click(function(){
      jqNoConflict('.'+this.value).toggle();
    });
  });
  </script>
  <script>
    var jqNoConflict = jQuery.noConflict();
jqNoConflict('#select-all').click(function(event) {
    if(this.checked) {
        // Iterate each checkbox
        jqNoConflict(':checkbox').each(function() {
            this.checked = true;
        });
    } else {
        jqNoConflict(':checkbox').each(function() {
            this.checked = false;
        });
    }
});
  </script>
