<?php
  function wcmeli_custom_query_vars_template5($vars) {
  $vars[] .= 'page';
  return $vars;
  }
  add_filter( 'query_vars', 'wcmeli_custom_query_vars_template5' );

  // an associative array containing the query var and its value
  $params = array('page' => 'importer-wcproducts');
  if(!class_exists('flux')) include WCMELI_PLUGIN_PATH.'wcml_v2/include/sa2_flux/include/flux.php';

  $flux = new flux('wcmeli_product');

  if(!file_exists(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')) file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode([])));
  $wcxml = (array)json_decode($flux->fluxiCode(file_get_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb')),true);

  if(!class_exists('meliProduct')) require 'wcml_v2/include/all_include.php';

  $wcmeliProduct = new meliProduct;

  if(isset($_POST['select'])) :
    foreach ($_POST as $key => $value) :
      if($key != 'select' && $key != 'select-all') :
        $product = wc_get_product($value);
        if(!$product->get_stock_quantity() && $product->get_stock_status() == 'instock') $qty = 1;
        elseif($product->get_stock_status() == 'onbackorder') $qty = 0;
        else $qty = $product->get_stock_quantity();

        if(strlen(wp_get_attachment_url($product->get_image_id())) < 2) :
          $mainImage = 'https://sa2.com.br/wcsa2meli/defaul_image.png';
        else :
          $mainImage = wp_get_attachment_url($product->get_image_id());
        endif;

        $productsData[] = [
          'wcid'            => $value,
          'sku'             => $product->get_sku(),
          'qty'             => $qty,
          'brand'           => WCMELI_BRAND,
          'title'           => $product->get_name(),
          'price'           => $product->get_price(),
          'images'          => [
            'source'        => $mainImage
          ],
          'categoryId'      => $wcmeliProduct->meliGetProductCategory($product->get_name())['body']->id,
          'description'     => $product->get_description(),
        ];
      endif;
    endforeach;
    // echo "<pre>";
    // var_dump($productsData);
    // exit;

    include 'wcml_v2/includes/wcmeli_get.php';
    if(isset($return)) :
      $productAttributes['productList'] = $productsData;
      $productAttributes['token'] = $wcmeliProduct->accessToken;
      $productAttributes['f'] = get_site_url();
      $productData = $wcmeliProduct->updatewcsa2meli(json_encode($productAttributes),base64_decode($return[0]),base64_decode($return[1]));

      $body = array(
          'wccpl' => $productData
      );

      $args = array(
          'body' => $body,
          'timeout' => '600',
          'redirection' => '5',
          'httpversion' => '1.0',
          'blocking' => true,
          'headers' => array(),
          'cookies' => array()
      );
      $url = 'https://sa2.com.br/wcsa2meli/wcmeli.php';
      $result = wp_remote_retrieve_body(wp_remote_post($url, $args));
      // echo "<pre>";
      // var_dump($result);
      // exit;
      $result = (array)json_decode($result,true);
      // echo "AQUIIIIII";
      // var_dump($result);
      // exit;
      if(isset($result['createProducts'])) :
        foreach ($result['createProducts'] as $key => $value) :
          if(!$value['wcsku']) :
            $wcxml[] = [
              'id' => end($wcxml)['id']+1,
              'wc' => $value['wcid'],
              'wc_type' => 'id',
              'mlb' => $value['id'],
              'mlb_type' => 'id'
            ];
            file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode($wcxml)));
          else :
            $wcxml[] = [
              'id' => end($wcxml)['id']+1,
              'wc' => $value['wcsku'],
              'wc_type' => 'sku',
              'mlb' => $value['id'],
              'mlb_type' => 'id'
            ];
            file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/wcxml.mlb',$flux->fluxCode(json_encode($wcxml)));
          endif;
          if(isset($value['defaultCategory'])) :
            add_action( 'admin_notices', 'wcmeliDefaultCategoryProduct' );
          endif;
          $created[] = $value['name'];
        endforeach;
      endif;

      if(isset($created)) :
        add_settings_error( 'sa2meliproduct_success', 'sa2meliproduct_success', __( count($created).' produtos foram criados com sucesso.' ),'success' );
        settings_errors( 'sa2meliproduct_success' );
      endif;

      if(isset($result['MessageError'])) :
        foreach ($result['MessageError'] as $key => $value) :
          add_settings_error( 'sa2meliproduct_error'.$key, 'sa2meliproduct_error', __( 'O produto '.$value['produto'].' não foi criado. '.$value['erro'] ) );
          settings_errors( 'sa2meliproduct_error'.$key );
        endforeach;
      endif;
    endif;
  endif;

  $args = array(
    'posts_per_page' => -1,
    'return'				=> 'ids',
    'simple'				=> true
  );
  $productIds = wc_get_products($args);
  $mlProductIds = $wcmeliProduct->meliGetProducts();
?>
  <div class="wrap" id="unique">
    <h1 class="wcmeli_title"><?php echo __( 'Exportar Produtos do Woocommerce para o Mercado Livre'); ?></h1>
      <input type="hidden" name="action" value="answer">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <form method="post" action="<?php echo add_query_arg($params, 'admin.php');?>" novalidate="novalidate">

              <?php $runs = 0;
              foreach ($productIds as $key => $value) :
                $wcProdInfo = wc_get_product($value);

                if( $wcProdInfo->get_type() == 'simple' ) :
                  $wcProdType = true;
                  $found = false;
                  if(count($wcxml) > 0) :
                    foreach ($wcxml as $_key => $_value) :
                      if($_value['wc_type'] == 'id' && $_value['wc'] == $value ||
                         $_value['wc_type'] == 'sku' && $_value['wc'] == $wcProdInfo->get_sku()) $found = true;
                    endforeach;
                  endif;
                  $mlProdNotExist = true;
                  // foreach ($mlProductIds as $_key => $_value) :
                  //   $sku = $wcmeliProduct->meliGetProductSku($_value);
                  //   if($wcProdInfo->get_sku() && $wcProdInfo->get_sku() == $sku) $mlProdNotExist = false;
                  // endforeach;
                else :
                  $wcProdType = false;
                endif;

                if( $wcProdType && $mlProdNotExist && !$found) : ?>
                  <?php if($runs == 0) : ?>
                    <div id="select-all-products">
                      <div style="border:1px solid #0062CC;margin:10px;padding:10px;">
                        <input type="checkbox" name="select-all" id="select-all" /><span class="wcmeli_title"><strong><?php echo __( 'Selecionar Todos' ); ?></strong></span>
                      </div>
                    </div>
                    <?php $runs++;?>
                  <?php endif; ?>
                  <?php $haveProducts = true; ?>
                  <div id="all-products">
                    <div id="product" style="border:1px solid #0062CC;margin:10px;padding:10px">
                      <div value="<?php echo __($value); ?>" class="product-<?php echo esc_attr($value); ?>">
                        <input type="checkbox" name="<?php echo __($value); ?>" value="<?php echo __($value); ?>"><span class="wcmeli_title"><?php echo __($wcProdInfo->get_name()); ?></span>
                        <button type="button" value="hidden-prodinfo<?php echo $key;?>" class="button toggle">Mostrar/Esconder Resposta</button>
                      </div>
                      <div class="hidden-prodinfo<?php echo $key;?>" style="display:none">
                        <table class="wp-list-table widefat fixed striped posts">
                          <tbody id="the-list">
                            <tr>
                              <th scope="row">
                                <div id="gallery">
                                  <?php if(!empty($wcProdInfo->get_image_id())) : ?>
                                    <div id="main-picture" >
                                      <img src="<?php echo wp_get_attachment_url( $wcProdInfo->get_image_id() ); ?>" width="224" height="224"/>
                                    </div>
                                  <?php else : ?>
                                    <div id="main-picture" >
                                      <img src="https://sa2.com.br/wcsa2meli/produto-sem-imagem.jpg" src="" width="224" height="224"/>
                                    </div>
                                  <?php endif; ?>
                                  <?php if(!empty($wcProdInfo->get_gallery_attachment_ids())) : ?>
                                    <?php foreach ($wcProdInfo->get_gallery_attachment_ids() as $key => $_pictureIds) : ?>
                                      <?php if($key == 0) : ?>
                                        <div id="secondary-pictures">
                                      <?php endif; ?>
                                      <?php if($key < 3) : ?>
                                        <img class="secondary-pictures" src="<?php echo wp_get_attachment_url( $_pictureIds ); ?>" width="70" height="70"/>
                                      <?php endif; ?>
                                      <?php if(count($wcProdInfo->get_gallery_attachment_ids() ) < 3 && count($wcProdInfo->get_gallery_attachment_ids() ) == $key+1) : ?>
                                        <?php for($i=$key+1; $i < 3; $i++) : ?>
                                          <img src="https://sa2.com.br/wcsa2meli/produto-sem-imagem.jpg" class="secondary-pictures" src="" width="70" height="70"/>
                                        <?php endfor; ?>
                                      <?php endif; ?>
                                      <?php if(count($wcProdInfo->get_gallery_attachment_ids() ) <= 3 && $key+1 == count($wcProdInfo->get_gallery_attachment_ids() ) || $key == 3) : ?>
                                          </div>
                                      <?php endif; ?>
                                    <?php endforeach; ?>
                                  <?php else : ?>
                                    <div id="secondary-pictures">
                                      <img src="https://sa2.com.br/wcsa2meli/produto-sem-imagem.jpg" class="secondary-pictures" src="" width="70" height="70"/>
                                      <img src="https://sa2.com.br/wcsa2meli/produto-sem-imagem.jpg" class="secondary-pictures" src="" width="70" height="70"/>
                                      <img src="https://sa2.com.br/wcsa2meli/produto-sem-imagem.jpg" class="secondary-pictures" src="" width="70" height="70"/>
                                    </div>
                                  <?php endif; ?>
                                </div>
                                <div id="price">
                                  <span class='wcmeli_title'>R$<?php echo number_format($wcProdInfo->get_price(),2, ',', ''); ?></span><br>
                                </div>
                                <div id="stock">
                                  <span class='wcmeli_title'>Estoque: <?php echo $wcProdInfo->get_stock_quantity(); ?></span><br>
                                </div>
                              </th>
                              <td>
                                <div id="description">
                                  <span class='wcmeli_title'><?php echo $wcProdInfo->get_description(); ?></span><br>
                                </div>
                                <div id="link">
                                  <span class='wcmeli_title'><a target="_blank" href="<?php echo $wcProdInfo->get_permalink(); ?>"><?php echo $wcProdInfo->get_permalink(); ?></a></span><br>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                <?php endif;?>
              <?php endforeach; ?>
              <?php if(!isset($haveProducts)) : ?>
                <div id="all-products" class="painel nonquestions">
                  <h1 class="wcmeli_title title"><?php echo __( 'TODOS OS PRODUTOS FORAM CRIADOS'); ?></h1>
                </div>
              <?php else : ?>
              <div class="row submit">
                <input type="submit" name="select" id="select" class="button button-large button-primary" value="Importar Selecionados" style="width:100%">
                <!-- <input type="submit" name="all" id="all" class="button button-large button-primary" value="Importar Todos" style="width:49%"> -->
              </div>
              <?php endif; ?>
            </form>
          </div>
        </div>
      </div>
  </div>
  <script>
  var jqNoConflict = jQuery.noConflict();
  jqNoConflict(document).ready(function(){
    jqNoConflict("button").click(function(){
      jqNoConflict('.'+this.value).toggle();
    });
  });
  </script>
  <script>
    var jqNoConflict = jQuery.noConflict();
    jqNoConflict('#select-all').click(function(event) {
    if(this.checked) {
        // Iterate each checkbox
        jqNoConflict(':checkbox').each(function() {
            this.checked = true;
        });
    } else {
        jqNoConflict(':checkbox').each(function() {
            this.checked = false;
        });
      }
    });
  </script>
