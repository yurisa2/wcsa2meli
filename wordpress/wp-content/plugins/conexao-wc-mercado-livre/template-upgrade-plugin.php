<?php function wcmeli_custom_query_vars_filter9($vars) {
  $vars[] .= 'page';
  return $vars;
}
add_filter( 'query_vars', 'wcmeli_custom_query_vars_filter9' );

// an associative array containing the query var and its value
$params = array('page' => 'upgrade-plugin');

if ( isset ( $_POST['action'] )  ) {
  if(!class_exists('meliProduct')) require 'wcml_v2/include/all_include.php';

  $wcmeliProduct = new meliProduct;
  include 'wcml_v2/includes/wcmeli_get.php';
  if(isset($return)) :
    $productAttributes['token'] = $wcmeliProduct->accessToken;
    $productAttributes['f'] = get_site_url();
    $productData = $wcmeliProduct->updatewcsa2meli(json_encode($productAttributes),base64_decode($return[0]),base64_decode($return[1]));

    $body = array(
        'upfull' => $productData
    );

    $args = array(
        'body' => $body,
        'timeout' => '600',
        'redirection' => '5',
        'httpversion' => '1.0',
        'blocking' => true,
        'headers' => array(),
        'cookies' => array()
    );
    $url = 'https://sa2.com.br/wcsa2meli/wcmeli.php';
    $result = wp_remote_retrieve_body(wp_remote_post($url, $args));
    $result = (array)json_decode($result,true);

    if(!isset($result['upfull']) && $result[0] == '-1') {
      add_settings_error( 'conexaowcmlupgrade_warning', 'conexaowcmlupgrade_warning', __( 'Ainda não visualizamos seu pagamento. O processo pode demorar algumas horas. Caso tenha já tenha pago, pedimos que tente novamente mais tarde ou entre em contato.' ),'warning' );
      settings_errors( 'conexaowcmlupgrade_warning' );
    } else {
      file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/includes/wcmeli_order.php',$result['upfull']['o']);
      file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/includes/wcmeli_product_images.php',$result['upfull']['pi']);
      file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/includes/wcmeli_grouped_product.php',$result['upfull']['gp']);
      file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/includes/wcmeli_external_product.php',$result['upfull']['ep']);
      file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/includes/wcmeli_variable_product.php',$result['upfull']['vp']);
      file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/includes/wcmeli_order_questions_sales.php',$result['upfull']['oqs']);

      add_settings_error( 'conexaowcmlupgrade_success', 'conexaowcmlupgrade_success', __( 'O plugin foi atualizado com sucesso!' ),'success' );
      settings_errors( 'conexaowcmlupgrade_success' );
    }
  endif;
}
?>
<div class="wrap" id="unique">
  <h1 class="wcmeli_title"><?php echo __( 'Faça o Upgrade agora!' ); ?></h1>
  <form method="post" action="<?php echo add_query_arg($params, 'admin.php');?>" novalidate="novalidate" onsubmit="return validateInputs()">
    <input type="hidden" name="action" value="upgrade">
    <center>
      <p>
        Para utilizar esse recurso, basta fazer a compra do plugin acessando o site sa2.com.br/safraweb#planos.

        Assim que o pagamento for aprovado, poderá fazer o upgrade manualmente do plugin que adicionará recursos adicionais.

        Ou esperar no máximo 24hrs para o upgrade automático.
      </p>
      <a target="_blank" class="button button-large button-primary" style="width:75%;margin-bottom:100px;" href="https://sa2.com.br/safraweb#planos">Comprar</a>

      <p>
        Caso queira fazer o upgrade manualmente e já usufruir dos novos recursos do plugin, como a sincronização e gerenciamento dos pedidos no mercado livre basta clicar no botão abaixo
      </p>
      <input type="submit" name="large" id="large" class="button button-large button-primary" value="Já comprei!" style="width:75%">
    </center>
  </form>
</div>
