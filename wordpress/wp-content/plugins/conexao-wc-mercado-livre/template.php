<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class WCMELI_Template
{

  public function __construct() {
        add_action( 'admin_menu', array( $this, 'wcmeli_add_menu' ));
        register_activation_hook( __FILE__, array( $this, 'wcmeli_install' ) );
        register_deactivation_hook( __FILE__, array( $this, 'wcmeli_uninstall' ) );
    }

    /*
      * Actions perform at loading of admin menu
      */
  public function wcmeli_add_menu() {
    $DB_WCMELI_Manager = new DB_WCMELI_Manager;
    $config_sa2_meliOption = $DB_WCMELI_Manager->wcsa2meliGetOptions();
        add_menu_page( 'upgradeplugin', 'Upgrade Conexão WC ML', 'manage_options', 'upgrade-plugin', array(
                          __CLASS__,
                         'wcmeli_page_file_path'
                        ));
        add_submenu_page( 'upgrade-plugin', 'upgradeplugin' . 'Faça o Upgrade agora!', ' Faça o Upgrade agora!', 'manage_options', 'upgrade-plugin', array(
                              __CLASS__,
                             'submenu_page_upgrade_plugin'
                            ));
        add_menu_page( 'wcmeli', 'WC to Mercado Livre', 'manage_options', 'advanced-settings', array(
                          __CLASS__,
                         'wcmeli_page_file_path'
                        ));
        add_submenu_page( 'advanced-settings', 'wcmeli' . 'Configurações Avançadas', ' Configurações Avançadas', 'manage_options', 'advanced-settings', array(
                              __CLASS__,
                             'submenu_page_settings1'
                            ));

        add_submenu_page( 'advanced-settings', 'wcmeli' . ' Conexão com Mercado Livre', 'Conexão com Mercado Livre', 'manage_options', 'token-ml', array(
                              __CLASS__,
                             'submenu_page_settings2'
                            ));
        if(isset($config_sa2_meliOption['prodquestion']) && $config_sa2_meliOption['prodquestion'] === true) :
          add_menu_page( 'meliautoresponder', 'Perguntas nos Anúncios', 'manage_options', 'auto-responder', array(
                            __CLASS__,
                           'wcmeli_page_file_path'
                          ));

          add_submenu_page( 'auto-responder', 'meliautoresponder' . 'Perguntas e Respostas', ' Perguntas e Respostas', 'manage_options', 'auto-responder', array(
                                __CLASS__,
                               'menu_page_settings2'
                              ));
        endif;

        add_menu_page( 'importermlproducts', 'Importar Produtos do Mercado Livre', 'manage_options', 'importer-mlproducts', array(
                          __CLASS__,
                         'wcmeli_page_file_path'
                        ));

        add_submenu_page( 'importer-mlproducts', 'importermlproducts' . 'Mercado Livre -> Woocommerce', ' Mercado Livre -> Woocommerce', 'manage_options', 'importer-mlproducts', array(
                              __CLASS__,
                             'menu_page_settings3'
                            ));

        add_menu_page( 'importerwcproducts', 'Exportar Produtos do Woocommerce', 'manage_options', 'importer-wcproducts', array(
                          __CLASS__,
                         'wcmeli_page_file_path'
                        ));

        add_submenu_page( 'importer-wcproducts', 'importerwcproducts' . 'Woocommerce -> Mercado Livre', ' Woocommerce -> Mercado Livre', 'manage_options', 'importer-wcproducts', array(
                              __CLASS__,
                             'menu_page_settings5'
                            ));

        // add_menu_page( __( 'WooCommerce', 'woocommerce' ), __( 'WooCommerce', 'woocommerce' ), 'manage_woocommerce', 'woocommerce', null, null, '55.5' );

        add_submenu_page( 'edit.php?post_type=product', __( 'Conectar', 'woocommerce' ), __( 'Conectar', 'woocommerce' ), 'manage_product_terms', 'conectar', array( $this, 'conectar_page' ) );
    }

    /**
    * Render submenu
    * @return void
    */
    static function submenu_page_upgrade_plugin() {
      include_once WCMELI_PLUGIN_PATH .'template-upgrade-plugin.php' ;
    }

    /**
    * Render submenu
    * @return void
    */
    static function submenu_page_settings1() {
      include_once WCMELI_PLUGIN_PATH .'template-1.php' ;
    }

    /**
     * Render submenu
     * @return void
     */
    static function submenu_page_settings2() {
        include_once WCMELI_PLUGIN_PATH .'template-2.php' ;
    }

    /**
     * Render submenu
     * @return void
     */
    static function menu_page_settings2() {
        include_once WCMELI_PLUGIN_PATH .'template-3.php' ;
    }

    /**
     * Render submenu
     * @return void
     */
    static function menu_page_settings3() {
        include_once WCMELI_PLUGIN_PATH .'template-4.php' ;
    }

    /**
     * Render submenu
     * @return void
     */
    static function menu_page_settings5() {
        include_once WCMELI_PLUGIN_PATH .'template-5.php' ;
    }

    /**
     * Render submenu
     * @return void
     */
    static function conectar_page() {
        include_once WCMELI_PLUGIN_PATH .'conectar-page.php' ;
    }


    /*
     * Actions perform on loading of menu pages
     */
    static function wcmeli_page_file_path() {



    }

    /*
     * Actions perform on activation of plugin
     */
    public function wcmeli_install() {



    }

    /*
     * Actions perform on de-activation of plugin
     */
    public function wcmeli_uninstall() {



    }

}
new WCMELI_Template;
