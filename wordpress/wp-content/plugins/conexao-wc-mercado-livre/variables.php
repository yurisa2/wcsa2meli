<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

if(!class_exists( 'DB_WCMELI_Manager' )) include_once WCMELI_PLUGIN_PATH. "db.php";

$DB_WCMELI_Manager = new DB_WCMELI_Manager;

$tokenHash = $DB_WCMELI_Manager->config_sa2_meli['token'];
if(!empty($tokenHash)) {
  $objToken  = (array)json_decode(base64_decode($tokenHash),true);
  $user_id  = $objToken['userid'];
}
$site_id    = "MLB";

include_once WCMELI_PLUGIN_PATH. "wcml_v2/include/apimeliwrapperphp/include/defines.php";

$pathfiles  = WCMELI_PLUGIN_PATH."wcml_v2/include/files/";

$settingsPriceMultiplication  = (int)$DB_WCMELI_Manager->config_sa2_meli['multprice'];
$settingPriceAddition         = (float)$DB_WCMELI_Manager->config_sa2_meli['addprice'];
$settingsStock                = (-1*(int)$DB_WCMELI_Manager->config_sa2_meli['settingsstock'])/100;
$preffixProd                  = '';
$suffixProd                   = $DB_WCMELI_Manager->config_sa2_meli['suffixprod'];
$brand                        = $DB_WCMELI_Manager->config_sa2_meli['prodbrand'];
$commissionValue              = (int)$DB_WCMELI_Manager->config_sa2_meli['commission'];
$commission                   = $commissionValue / 100;


//CHAVES PARA SINCRONIZAÇÃO
$syncProd     = $DB_WCMELI_Manager->config_sa2_meli['syncprod'];
$title        = $DB_WCMELI_Manager->config_sa2_meli['attprodtitle'];
$price        = $DB_WCMELI_Manager->config_sa2_meli['attprodprice'];
$description  = $DB_WCMELI_Manager->config_sa2_meli['attproddesctiption'];
$stock        = $DB_WCMELI_Manager->config_sa2_meli['attprodstock'];
$images       = $DB_WCMELI_Manager->config_sa2_meli['attprodimages'];
$order        = $DB_WCMELI_Manager->config_sa2_meli['syncorder'];

$updateImages = (int)$DB_WCMELI_Manager->config_sa2_meli['syncimages'];
$sendEmail    = $DB_WCMELI_Manager->config_sa2_meli['errortime'];

$configmail             = (bool)$DB_WCMELI_Manager->config_sa2_meli['configmail'];                   // true para habilitar o envio de email
$emailTo                = explode(',',$DB_WCMELI_Manager->config_sa2_meli['emailerror']);
$emailNewSale           = explode(',',$DB_WCMELI_Manager->config_sa2_meli['emailnewsale']);
$emailNewQuestion       = explode(',',$DB_WCMELI_Manager->config_sa2_meli['emailproductquestion']);
$emailQuestionAfterSale = explode(',',$DB_WCMELI_Manager->config_sa2_meli['emailaftersale']);

$SendEmailNewQuestion = $DB_WCMELI_Manager->config_sa2_meli['questiontime'];          //tempo em segundos para mandar email de notificação de perguntas do ml

// Ainda há problemas não encontrados para o uso do Sendmail
$SMTP = (bool)$DB_WCMELI_Manager->config_sa2_meli['smtp'];              //se SMTP é false, sendmail será usado

//se SMTP true as variaveis abaixo precisam ser setadas e configuradas
$Host       = $DB_WCMELI_Manager->config_sa2_meli['host'];  // Specify main and backup SMTP servers
$SMTPAuth   = $DB_WCMELI_Manager->config_sa2_meli['auth'];                               // Enable SMTP authentication
$Username   = $DB_WCMELI_Manager->config_sa2_meli['username'];         // SMTP username
$Password   = $DB_WCMELI_Manager->config_sa2_meli['password'];                       // SMTP password
$SMTPSecure = $DB_WCMELI_Manager->config_sa2_meli['secure'];                          // Enable TLS encryption, `ssl` also accepted

$from_mail  = $DB_WCMELI_Manager->config_sa2_meli['from_mail'];
$from_title = $DB_WCMELI_Manager->config_sa2_meli['from_title'];

$prodquestion = $DB_WCMELI_Manager->config_sa2_meli['prodquestion'];
$autoprodquestionfrete = $DB_WCMELI_Manager->config_sa2_meli['autoprodquestionfrete'];
?>
