<?php
 if(!isset($prefix_meli)) $prefix_meli = '';

if(!class_exists( 'Meli' )) {
  include_once __DIR__ .$prefix_meli.'/ml/php-sdk/Meli/meli.php';
  include_once __DIR__ .$prefix_meli.'/ml/php-sdk/configApp.php';
}

if(file_exists(__DIR__ .$prefix_meli.'/defines.php')) include_once __DIR__ .$prefix_meli.'/defines.php';

if(file_exists(__DIR__ .$prefix_meli.'/meli.php')) include_once __DIR__ .$prefix_meli.'/meli.php';
if(file_exists(__DIR__ .$prefix_meli.'/products.php')) include_once __DIR__ .$prefix_meli.'/products.php';
if(file_exists(__DIR__ .$prefix_meli.'/orders.php')) include_once __DIR__ .$prefix_meli.'/orders.php';
?>
