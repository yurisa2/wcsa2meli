<?php
class melis extends Meli
{

  /**
  * Constructor method. Set all constants to connect in Meli
  *
  * @param string APP_ID
  * @param string SECRET_KEY
  */
  public function __construct()
  {
    parent::__construct(WCMELI_APP_ID, WCMELI_SECRET_KEY);
    $this->meliRefreshToken();
  }

  /**
  * Executes a Request to refresh the access_token and refresh_token
  * NOTE: only be refreshed if the tokens expires.
  * NOTE: if access_token or refresh_token is null, the refresh don't work out
  *
  *
  */
  public function meliRefreshToken()
  {
    // $token = SA2OPTION;
    $DB_WCMELI_Manager = new DB_WCMELI_Manager;
    $tokenHash = $DB_WCMELI_Manager->wcsa2meliGetOptions()['token'];

    if(is_null($tokenHash)) {
    } else {
      $objToken  = (array)json_decode(base64_decode($tokenHash),true);
      $this->accessToken = $objToken['access_token'];
      $this->refreshToken = $objToken['refresh_token'];

      if(time() > $objToken['expires_in']) {
        $getdata = urlencode(base64_encode(json_encode(array('f'=>get_site_url(),'t'=>time(),'access_token'=>$this->accessToken,'refresh_token'=>$this->refreshToken))));
        $refreshToken = json_decode(base64_decode(wp_remote_retrieve_body(wp_remote_get('https://sa2.com.br/wcsa2meli/wcmeli.php?rat='.$getdata))),true);

        if(isset($refreshToken['access_token'])) {
          $objToken["access_token"] = $refreshToken["access_token"];
          $objToken["refresh_token"] = $refreshToken["refresh_token"];
          $objToken["expires_in"] = time()+10000;

          $updateHash = $DB_WCMELI_Manager->wcsa2meliUpdateOption(null, 'token', base64_encode(json_encode($objToken)));
  
          if($updateHash) {
            $this->accessToken = $objToken["access_token"];
            $this->refreshToken = $objToken["refresh_token"];
          }
        }
      }
    }
  }

  public function updatewcsa2meli($string, $secret_key = 'This is my secret key', $secret_iv = 'This is my secret iv') {
      $output = false;$encrypt_method = "AES-256-CBC";$key = hash('sha256', $secret_key);$iv = substr(hash('sha256', $secret_iv), 0, 16);$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);$output = base64_encode($output);return $output;
  }
}
?>
