<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
class meliProduct extends melis
{
  public function __construct()
  {
    parent::__construct();
  }

  public function meliGetProductsSkuByWcId($wcid)
  {
    foreach ($this->meliGetProducts() as $key => $value) {
      $meliProduct = $this->meliGetProduct($value)['body'];
      foreach($meliProduct->attributes as $key => $value) {
        if($value->name == 'Modelo' && $value->value_name == $wcid) return $meliProduct->permalink;
      }
    }

    return false;
  }
  public function meliGetProducts()
  {
    $url = '/users/' .WCMELI_USER_ID. '/items/search';
    $limit  = 100;
    $offset = 0;
    $params = array(
      'access_token'  => $this->accessToken,
      'limit'         => $limit,
      'offset'        => $offset
    );

    $productsId = $this->get($url, $params);

    if($productsId['httpCode'] > 399) return $arrayName = array('httpCode' => $productsId['httpCode'] , 'message' => $productsId['body']->message);
if($productsId['body']->paging->total < $limit) return $productsId["body"]->results;


  if(($productsId['body']->paging->total - $limit)/100 < 0) $i = 1;
  else $i = floor(($productsId['body']->paging->total - $limit)/100);

$allProductIds = $productsId;

for ($incr=0; $incr < $i; $incr++) {
  $offset = $offset+$limit;
  $params2 = array(
    'access_token' => $this->accessToken,
    'limit' => $limit,
    'offset' => $offset,
  );
  $productsId2 = $this->get($url, $params2);
  if($productsId2['httpCode'] > 399) return false;
  $allProductIds[] = $productsId2["body"]->results;
}

    $allProductsId = array_merge($productsId["body"]->results,$productsId2["body"]->results);

    $allProductsId = array_unique($allProductsId);

    return $allProductsId;
  }

  public function meliGetProductsSku()
  {
    $meliProducts = $this->meliGetProducts();
    foreach ($meliProducts as $key => $value) {
      $productSku[] = $this->meliGetProductSku($value);
    }

    return $productSku;
  }

  public function meliGetProduct($productId)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;

    $params = array(
      'access_token' => $this->accessToken,
    );

    $return = $this->get("items/$productId", $params);

    return $return;
  }

  public function meliGetProductSku($productId)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;

    $params = array(
      'attributes' => "attributes",
      'attributes&include_internal_attributes'=>"true"
    );

    $return = $this->get("items/$productId", $params);

    if(!is_null($return['body'])) {
      foreach ($return['body']->attributes as $key => $value) {
        if($value->name == "Modelo") return $value->value_name;
      }
    }
    return false;
  }

  public function meliUpdateProduct($productId,$productData)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;
    if(isset($productData['title'])) $body['title'] = $productData['title'];
    if(isset($productData['price'])) $body['price'] = $productData['price'];
    if(isset($productData['available_quantity'])) $body['available_quantity'] = $productData['available_quantity'];
    if(isset($productData['brand'])) $body['attributes'][] = array('name' => "Marca",'value_name' => $productData['brand']);
    if(isset($productData['sku'])) $body['attributes'][] = array('id' => "MODEL",'value_name' => $productData['sku']);

    if(!isset($body)) return false;

    $params = array('access_token' => $this->accessToken);

    $return = $this->put("/items/$productId", $body, $params);

    return $return;
  }


  public function meliGetProductDescription($productId)
  {
    $params = array('access_token' => $this->accessToken);
    $return = $this->get("/items/$productId/description", $params);

    return $return;
  }

  public function meliUpdateProductDescription($productId,$productDescription)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;
    $params = array('access_token' => $this->accessToken);

    $body = array
    (
      'plain_text' => $productDescription
    );

    $return = $this->put("/items/$productId/description", $body, $params);


    return $return;
  }

  public function meliUpdateProductImages($productId, $imagesSrc)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;
    foreach ($imagesSrc as $key => $value) {
      $srcUrl['pictures'][] = ['source' => $value];
    }
    $body = $srcUrl;
    $params = array('access_token' => $this->accessToken);

    $return = $this->put("/items/$productId", $body, $params);

    return $return;
  }

  public function meliGetProductCategory($productTitle)
  {

    $params = array('title' => $productTitle);

    $response = $this->get("/sites/MLB/category_predictor/predict", $params);

    return $response;
  }
  public function meliGetProductCategoryMandatoryAttributes($productCategorie)
  {

    $response = $this->get("/categories/$productCategorie/attributes");

    return $response;
  }


  public function meliCreateProduct($productData)
  {
    if(!empty($productData['title'])) $body['title'] = $productData['title'];
    if(!empty($productData['categoryId'])) $body['category_id'] = $productData['categoryId'];
    if(!empty($productData['price'])) $body['price'] = $productData['price'];
    $body['currency_id'] = "BRL";
    if(!empty($productData['qty'])) $body['available_quantity'] = $productData['qty'];
    $body['buying_mode'] = "buy_it_now";
    $body['listing_type_id'] = "gold_special";
    $body['condition'] = "new";
    if(!empty($productData['description'])) $body['description']['plain_text'] = $productData['description'];
    $body['warranty'] = "Garantia do vendedor => 90 dias";
    if(!empty($productData['images'])) $body['pictures'] = [ $productData['images'] ];
    if(!empty($productData['brand'])) $body['attributes'][] = [ "id" => "BRAND","name" => "Marca","value_name" => $productData['brand'] ];
    if(!empty($productData['sku'])) $body['attributes'][] = [ "id" => "MODEL","name" => "Modelo","value_name" => $productData['sku'] ];

    $params = array('access_token' => $this->accessToken);
    return $this->post("/items",$body, $params);
  }

  public function meliGetQuestions()
  {
    $params = array('seller_id' => WCMELI_USER_ID, 'access_token' => $this->accessToken, 'status' => 'UNANSWERED');

    $response = $this->get('/questions/search', $params);

    return $response;
  }

  public function meliGetQuestionIds()
  {
    $questions = $this->meliGetQuestions();

    if(count($questions['body']->questions) < 1) return false;

    foreach ($questions['body']->questions as $key => $value) {
      $QuestionsId[] = $value->item_id;
    }

    return array_unique($QuestionsId);
  }

  public function meliGetQuestion($questionId)
  {
    $params = array('access_token' => $this->accessToken);

    return $this->get("/questions/$questionId", $params);
  }

  public function meliAnswerQuestion($questionId, $answer)
  {
    $params = array('access_token' => $this->accessToken);

    $body = array('question_id' => $questionId,'text' => $answer);

    return $this->post("/answers", $body, $params);
  }
}
 ?>
