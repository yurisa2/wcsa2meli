<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

define("PATHFILES",$pathfiles);
if(isset($user_id)) define("WCMELI_USER_ID",$user_id);

define("WCMELI_SETTINGS_PRICE_MULTIPLICATION",$settingsPriceMultiplication);
define("WCMELI_SETTINGS_PRICE_ADDITION",$settingPriceAddition);
define("WCMELI_SETTINGS_STOCK",$settingsStock);
define("WCMELI_SUFFIX_PROD",$suffixProd);
define("WCMELI_PREFFIX_PROD",$preffixProd);
define("WCMELI_BRAND",$brand);
define("WCMELI_COMMISSION",$commission);

//KEY TO SINCRONIZATION OF PRODUCTS AND ORDER
define("WCMELI_SYNCPROD",$syncProd);
define("WCMELI_TITLE",$title);
define("WCMELI_PRICE",$price);
define("WCMELI_DESCRIPTION",$description);
define("WCMELI_STOCK",$stock);
define("WCMELI_IMAGES",$images);
define("WCMELI_ORDER",$order);

define("WCMELI_UPDATE_IMAGES",$updateImages);
define("WCMELI_SEND_TIME",$sendEmail);

//MANDAR EMAIL
define("WCMELI_CONFIGMAIL",$configmail);     // true para habilitar o envio de email
define("WCMELI_EMAIL_TO",$emailTo);
define("WCMELI_EMAIL_NEWSALE",$emailNewSale);
define("WCMELI_EMAIL_NEWQUESTION",$emailNewQuestion);
define("WCMELI_EMAIL_QUESTIONAFTERSALE",$emailQuestionAfterSale);

define("WCMELI_SENDEMAIL_NEWQUESTION",$SendEmailNewQuestion);
// Ainda há problemas não encontrados para o uso do Sendmail
define("WCMELI_SMTP",$SMTP);     //if SMTP equals false, sendmail will be used

//if SMTP equals true, the variaveis below will need be set up
define("WCMELI_HOST",$Host);     // Specify main and backup SMTP servers
define("WCMELI_SMTPAUTH",$SMTPAuth);     // Enable SMTP authentication
define("WCMELI_USERNAME",$Username);     // SMTP username
define("WCMELI_PASSWORD",$Password);     // SMTP password
define("WCMELI_SMTPSECURE",$SMTPSecure);     // Enable TLS encryption, `ssl` also accepted


define("WCMELI_FROM_MAIL",$from_mail);
define("WCMELI_FROM_TITLE",$from_title);

define("WCMELI_PROD_QUESTION",$prodquestion);
define("WCMELI_AUTO_PROD_QUESTION_FRETE",$autoprodquestionfrete);
