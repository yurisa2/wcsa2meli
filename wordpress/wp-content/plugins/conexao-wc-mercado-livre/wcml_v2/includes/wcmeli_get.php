<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit("Não possui permissão"); // Exit if accessed directly
}

if(!file_exists(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/code')) {
  $getdata = urlencode(base64_encode(json_encode(array('f'=>get_site_url(),'t'=>time()))));
  $returnString = wp_remote_retrieve_body(wp_remote_get('https://sa2.com.br/wcsa2meli/wcmeli.php?req='.$getdata));

  if($returnString == '-1') {
    if(function_exists('add_settings_error')) {
      add_settings_error( 'conexaowcmlerror', 'conexaowcmlerror', __( 'Conexão Wc Mercado Livre: Seu acesso está bloqueado por algum motivo. Favor entre em contato conosco para saber mais informações!!' ),'error' );
      settings_errors( 'conexaowcmlerror' );
    } else {
      $log = new WC_Logger();
      $log_entry = 'Conexão Wc Mercado Livre: Seu acesso está bloqueado por algum motivo. Favor entre em contato conosco para saber mais informações!!';
      $log->add( 'wcmeli', $log_entry );
    }
  } else {
    file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/code',$returnString);
  }
}

if(file_exists(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/code')) {
  $returnString = file_get_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/code');

  $returnArray = explode(':',$returnString);
  $return = base64_decode($returnArray[0]);
  $data = $returnArray[1];
  if(time() > $data) {
    $getdata = urlencode(base64_encode(json_encode(array('f'=>get_site_url(),'t'=>time()))));
    $returnString = wp_remote_retrieve_body(wp_remote_get('https://sa2.com.br/wcsa2meli/wcmeli.php?req='.$getdata));

    if($returnString == '-1') {
      if(function_exists('add_settings_error')) {
        add_settings_error( 'conexaowcmlerror', 'conexaowcmlerror', __( 'Conexão Wc Mercado Livre: Seu acesso está bloqueado por algum motivo. Favor entre em contato conosco para saber mais informações!!' ),'error' );
        settings_erors( 'conexaowcmlerror' );
      } else {
        $log = new WC_Logger();
        $log_entry = 'Conexão Wc Mercado Livre: Seu acesso está bloqueado por algum motivo. Favor entre em contato conosco para saber mais informações!!';
        $log->add( 'wcmeli', $log_entry );
      }
    } else {
      file_put_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/code',$returnString);
      $returnString = file_get_contents(WCMELI_PLUGIN_PATH.'wcml_v2/include/files/code');
      $returnArray = explode(':',$returnString);
      $return = base64_decode($returnArray[0]);
      $data = $returnArray[1];
    }
  }
  if(isset($return)) $return = (array)json_decode($return,true);
}

// var_dump(isset($return));
// exit;
 ?>
