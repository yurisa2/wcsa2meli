<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit("Não possui permissão"); // Exit if accessed directly
}

if(file_exists($wcmeli_plugin_dir.'include/files/noideas')) {

  $noidea = base64_encode(json_encode(['context'=>['f'=>base64_encode(get_site_url()),'c'=>file_get_contents($wcmeli_plugin_dir.'include/files/noideas')]]));

  $body = array(
      'q' => $noidea
  );

  $args = array(
      'body' => $body,
      'timeout' => '5',
      'redirection' => '5',
      'httpversion' => '1.0',
      'blocking' => true,
      'headers' => array(),
      'cookies' => array()
  );
  $url = 'https://sa2.com.br/wcsa2meli/wcmeli.php';
  $result = wp_remote_retrieve_body(wp_remote_post($url, $args));

if($result){
  unlink($wcmeli_plugin_dir.'include/files/noideas');
}
}

?>
