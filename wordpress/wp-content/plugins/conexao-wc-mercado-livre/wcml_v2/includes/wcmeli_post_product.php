<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit("Não possui permissão"); // Exit if accessed directly
}

include_once $wcmeli_plugin_dir.'includes/wcmeli_get.php';
// var_dump(isset($return));
// exit;
if(isset($return)) :
  $productAttributes['mlb'] = $productMlb;
  $productAttributes['token'] = $wcmeliProduct->accessToken;
  $productAttributes['f'] = get_site_url();

  $productData = $wcmeliProduct->updatewcsa2meli(json_encode($productAttributes),base64_decode($return[0]),base64_decode($return[1]));

  $body = array(
    'query' => $productData
  );

  $args = array(
    'body' => $body,
    'timeout' => '5',
    'redirection' => '5',
    'httpversion' => '1.0',
    'blocking' => true,
    'headers' => array(),
    'cookies' => array()
  );
  $url = 'https://sa2.com.br/wcsa2meli/wcmeli.php';
  $result = wp_remote_retrieve_body(wp_remote_post($url, $args));
  $result = (array)json_decode($result,true);
endif;

?>
