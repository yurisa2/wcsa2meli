<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit("Não possui permissão"); // Exit if accessed directly
}
if(WCMELI_PROD_QUESTION) :
  $wcmeliProduct = new meliProduct;
  $fluxProductQuestion = new flux('wcmeli_product_question');
  $fluxProductQuestion->pathListItem = true;

  if(!$fluxProductQuestion->setFiles()) {
    $questionIds = $wcmeliProduct->meliGetQuestionIds();
    $fluxProductQuestion->list_item = $questionIds;
    $fluxProductQuestion->setFiles();
  }
  $fluxProductQuestion->getFiles();
  $productId = $fluxProductQuestion->next_item();

  if($productId != false) {

  $pergunta = $wcmeliProduct->meliGetQuestion($productId);

  if(empty($pergunta['body'])) exit;

  $pergunta = $pergunta['body'];
  if(WCMELI_AUTO_PROD_QUESTION_FRETE) :
    $isFreight = 0;
    if( strpos(strtolower($pergunta->text),'frete') ) {
      $isFreight += 5;
    }
    if( strpos(strtolower($pergunta->text),'frete para') ) {
      $isFreight += 6;
    }
    if( strpos(strtolower($pergunta->text),'cep') ) {
      $isFreight += 8;
    }
    if( preg_match("/\b\d{5}(-\d{4})?[0-9]{3}\b/", $pergunta->text,$matches1)) {
      $isFreight += 20;
    }
    if(preg_match("/\b\d{5}(-\d{4})?\-[0-9]{3}\b/", $pergunta->text,$matches2)) {
      $isFreight += 20;
    }

    if($isFreight >= 20) {
      if(empty($matches2)) $cepDest  = str_replace('-','',$matches1[0]);
      if(empty($matches1)) $cepDest  = str_replace('-','',$matches2[0]);

      $cepOrig  = str_replace('-','',get_option('woocommerce_store_postcode'));


      $meliSkuProduct = $wcmeliProduct->meliGetProductSku($pergunta->item_id);

      $wcProductId = wc_get_product_id_by_sku($meliSkuProduct);
      if($wcProductId < 1) $wcProductId = $meliSkuProduct;

      $productInfo = wc_get_product($wcProductId);
      if(!$productInfo) {
        $WC_Logger = new WC_Logger();
        $WC_Logger_entry .= __("Produto $productMlb (SKU: $productSku) não foi encontrado no WooCommerce", true );
        $WC_Logger->add( 'wcmeli', $WC_Logger_entry );
      } else {
        if(get_option('woocommerce_weight_unit') == 'kg') $peso = $productInfo->get_weight();
        if(get_option('woocommerce_weight_unit') == 'g') $peso = $productInfo->get_weight() / 100;
        if(get_option('woocommerce_weight_unit') == 'lbs') $peso = $productInfo->get_weight() / 2.205;
        if(get_option('woocommerce_weight_unit') == 'oz') $peso = $productInfo->get_weight() / 35.274;

        if(get_option('woocommerce_dimension_unit') == 'm') {
          $altura       = $productInfo->get_height() * 100;
          $largura      = $productInfo->get_width() * 100;
          $diametro     = 0;
          $comprimento  = $productInfo->get_length() * 100;
        }
        if(get_option('woocommerce_dimension_unit') == 'cm') {
          $altura       = $productInfo->get_height();
          $largura      = $productInfo->get_width();
          $diametro     = 0;
          $comprimento  = $productInfo->get_length();
        }
        if(get_option('woocommerce_dimension_unit') == 'mm') {
          $altura       = $productInfo->get_height() / 10;
          $largura      = $productInfo->get_width() / 10;
          $diametro     = 0;
          $comprimento  = $productInfo->get_length() / 10;
        }
        if(get_option('woocommerce_dimension_unit') == 'in') {
          $altura       = $productInfo->get_height() * 2.54;
          $largura      = $productInfo->get_width() * 2.54;
          $diametro     = 0;
          $comprimento  = $productInfo->get_length() * 2.54;
        }
        if(get_option('woocommerce_dimension_unit') == 'yd') {
          $altura       = $productInfo->get_height() * 91.44;
          $largura      = $productInfo->get_width() * 91.44;
          $diametro     = 0;
          $comprimento  = $productInfo->get_length() * 91.44;
        }
      }

    //mudar file_get_contents para wp_remote_get

      $stringServicePreco = 'nCdServico=41106&nCdEmpresa=&sDsSenha=&sCepOrigem='.$cepOrig.'&sCepDestino='.$cepDest.'&nVlAltura='.$altura.'&nVlLargura='.$largura.'&nVlDiametro='.$diametro.'&nVlComprimento='.$comprimento.'&nVlPeso='.$peso.'&nCdFormato=1&sCdMaoPropria=N&nVlValorDeclarado=0&sCdAvisoRecebimento=N&StrRetorno=xml';
      $stringServicePrazo = 'nCdServico=41106&sCepOrigem='.$cepOrig.'&sCepDestino='.$cepDest;

      // $stringService = implode('&',$arrayService);
      $correiosPreco = simplexml_load_string(wp_remote_retrieve_body(wp_remote_get('http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?'.$stringServicePreco)));
      $correiosPrazo = simplexml_load_string(wp_remote_retrieve_body(wp_remote_get('http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx/CalcPrazo?'.$stringServicePrazo)));

      if($correiosPreco->cServico->Erro != 0 || !empty($correiosPrazo->Servicos->cServico->Erro)) {
        if($correiosPreco->cServico->Erro == '-3') $resposta = 'Não foi possível realizar o cáculo pois CEP informado não foi encontrado.';
        else $autoAnswer = false;
      } else {
        $resposta = 'O frete fica no valor de R$'.$correiosPreco->cServico->Valor.' com prazo de '.$correiosPrazo->Servicos->cServico->PrazoEntrega.' dias e data máxima (para a entrega) dia '.$correiosPrazo->Servicos->cServico->DataMaxEntrega;
      }

      if(isset($resposta)) {
        $resposta = $wcmeliProduct->meliAnswerQuestion($productId, $resposta);
        if(empty($resposta['body'])) $autoAnswer = false;
        else $autoAnswer = true;
      }
        if(!$autoAnswer) {
          if($timerHelper['productQuestion'] + WCMELI_SENDEMAIL_NEWQUESTION <= time()) {
            $log = new WC_Logger();
            $log_entry .= __('Não foi possível responder automaticamente a pergunta. Enviando email...', true );
            $log->add( 'wcmeli', $log_entry );

            $log = new log("Nova Pergunta Mercado Livre", "Há nova(s) pergunta(s) no mercado livre que não puderam ser respondidas automaticamente", "", "nova pergunta");
            $log->log_email = true;
            $log->mensagem_email = "Nova Pergunta Mercado Livre";
            $log->dir_file = "log/log.json";
            $log->log_files = true;
            $log->email_pergunta = true;
            $log->send_log_email();
            $log->execute();
            $fluxProduct->addTimer('productQuestion',time());
          }
        } else {
          $log = new WC_Logger();
          $log_entry .= __('Pergunta Respondida automaticamente', true );
          $log->add( 'wcmeli', $log_entry );

          $log = new log("Pergunta Respondida automaticamente", "A pergunta ".strtoupper($pergunta->text)." foi respondida automaticamente.", "Resposta enviada: ".$resposta, "nova pergunta");
          $log->log_email = true;
          $log->email_newquestion = true;
          $log->log_email = true;
          $log->dir_file = "log/log.json";
          $log->log_files = true;
          $log->email_pergunta = true;
          $log->send_log_email();
          $log->execute();

          $fluxProductQuestion->add_item($productId);
        }
      } else {
        if($timerHelper['productQuestion'] + WCMELI_SENDEMAIL_NEWQUESTION <= time()) {
          $log = new log("Nova Pergunta Mercado Livre", "Há nova(s) pergunta(s) no mercado livre ", "", "nova pergunta");
          $log->log_email = true;
          $log->mensagem_email = "Nova Pergunta Mercado Livre";
          $log->dir_file = "log/log.json";
          $log->log_files = true;
          $log->email_pergunta = true;
          $log->send_log_email();
          $log->execute();
          $fluxProduct->addTimer('productQuestion',time());
        }
      }
    else :
      if($timerHelper['productQuestion'] + WCMELI_SENDEMAIL_NEWQUESTION <= time()) :
        $log = new WC_Logger();
        $log_entry .= __('Nova Pergunta Mercado Livre', true );
        $log->add( 'wcmeli', $log_entry );

        $log = new log("Nova Pergunta Mercado Livre", "Há nova(s) pergunta(s) no mercado livre", "", "nova pergunta");
        $log->log_email = true;
        $log->mensagem_email = "Nova Pergunta Mercado Livre";
        $log->dir_file = "log/log.json";
        $log->log_files = true;
        $log->email_pergunta = true;
        $log->send_log_email();
        $log->execute();
        $fluxProduct->addTimer('productQuestion',time());
      endif;
    endif;
  }
endif;
