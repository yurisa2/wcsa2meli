<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit("Não possui permissão"); // Exit if accessed directly
}

  $productName = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/"),explode(" ","a A"),$productInfo->get_title());
  if(strlen($productName) > 60) {
    $productName = explode(' ',$productName);
    array_pop($productName);
    $productName = implode(' ',$productName);
  }
  if(!strpos(strtolower($productName),strtolower(WCMELI_SUFFIX_PROD))) $title = WCMELI_PREFFIX_PROD.trim($productName).' '.trim(WCMELI_SUFFIX_PROD);
  else $title = WCMELI_PREFFIX_PROD.trim($productName);
  if (strlen($title) > 60) $title = WCMELI_PREFFIX_PROD.$productName;

  $settingPrice = ($productInfo->get_price() * WCMELI_SETTINGS_PRICE_MULTIPLICATION) + WCMELI_SETTINGS_PRICE_ADDITION;
  $price = round($productInfo->get_price() + ($productInfo->get_price() * WCMELI_COMMISSION) + $settingPrice ,2);

  $stock = floor($productInfo->get_stock_quantity() + ($productInfo->get_stock_quantity()*WCMELI_SETTINGS_STOCK));

  if($stock < 0 || $productInfo->get_stock_status() == 'onbackorder') $stock = 0;
  if($stock == 0 && $productInfo->get_stock_status() == 'instock') $stock = 1;

  $productMlDetails = $wcmeliProduct->meliGetProduct($productMlb)['body'];

  if($productMlDetails->sub_status[0] == 'deleted' || $productMlDetails->status == 404) {
    foreach ($wcxml as $key => $value) {
      if($value['mlb'] != $productMlb) {
        $syncProds[] = $value;
      }
    }
    var_dump(file_put_contents($wcmeli_plugin_dir.'include/files/wcxml.mlb',$flux->fluxCode(json_encode($syncProds))));
    exit;
  }

  $productMlDescription = $wcmeliProduct->meliGetProductDescription($productMlb)['body'];

  if(!is_null($productMlDetails)) {
  if($productMlDetails->listing_type_id != 'free' && $productMlDetails->condition == 'new') {

  if($productMlDetails->status != 'paused' || ($productMlDetails->status == 'paused' && $stock >= 0)) {

    if(WCMELI_TITLE && trim(strtolower($title)) != trim(strtolower($productMlDetails->title))) $productAttributes['product']['title'] = $title;


    if(WCMELI_PRICE && $price != $productMlDetails->price) $productAttributes['product']['price'] = $price;


    if(WCMELI_STOCK && $stock != $productMlDetails->available_quantity) $productAttributes['product']['available_quantity'] = (int)$stock;


    foreach ($productMlDetails->attributes as $key => $value) {
      if($value->id == 'BRAND') {

        $brand = true;
        if(WCMELI_BRAND != $value->value_name) {
          $productAttributes['product']['attributes'][] = array('name' => "Marca",'value_name' => WCMELI_BRAND);
        }
      }
      if($value->id == 'MODEL' && $productSku != $value->value_name) $productAttributes['product']['attributes'][] = array('id' => "MODEL",'value_name' => $productSku);
    }

    if(!isset($brand)) $productAttributes['product']['attributes'][] = array('name' => "Marca",'value_name' => WCMELI_BRAND);

    if($productMlDescription->plain_text != $productInfo->description) $productAttributes['description'] = $productInfo->description;

  if(isset($productAttributes)) {
    include $wcmeli_plugin_dir . 'includes/wcmeli_post_product.php';

    if(isset($result['product']['MessageError'])) {
      $fluxProduct = new flux('wcmeli_product');
      $fluxProduct->timeFile = true;
      $fluxProduct->setFiles();

      $fluxTimer = $fluxProduct->getTimer();

      $erroProduct = 'Produto';
      // Log any exceptions to a WC logger
      $log = new WC_Logger();
      $log_entry = 'Prod '.$productMlb.': ';
      $log_entry .= print_r( implode(' | ',$result['product']['MessageError']), true );
      $log->add( 'wcmeli', $log_entry );

      if($fluxTimer['product'] + WCMELI_SEND_TIME <= time()) {
        $error = new error_handling("Conexão Wc Mercado Livre: Informações do Produto", "Algo impossibilitou a atualização do produto SKU:$productSku - MLB:$productId", $result['product']['MessageError'], "Erro produto");
        $error->send_mail = true;
        $error->send_error_email();
        $error->execute();
        $fluxProduct->addTimer('product',time());
      }
    }

    if(isset($result['description']['MessageError'])) {
      $erroDescription = 'Descrição';
      // Log any exceptions to a WC logger
      $log = new WC_Logger();
      $log_entry = 'Prod '.$productMlb.': ';
      $log_entry .= print_r( implode(' | ',$result['description']['MessageError']), true );
      $log->add( 'wcmeli', $log_entry );

      if($fluxTimer['product'] + WCMELI_SEND_TIME <= time()) {
        $error = new error_handling("Conexão Wc Mercado Livre: Descrição do Produto", "Algo impossibilitou a atualização do produto SKU:$productSku - MLB:$productId", $result['description']['MessageError'], "Erro produto");
        $error->send_mail = true;
        $error->send_error_email();
        $error->execute();
        $fluxProduct->addTimer('product',time());
      }
    }
    if(isset($result['description']['success']) && isset($result['product']['success'])) {
      $log = new WC_Logger();
      $log_entry = print_r( 'Produto '.$productMlb .' atualizado com sucesso', true );
      $log->add( 'wcmeli', $log_entry );
      echo "<br>atualizado com sucesso<br>";
    }
  } else echo "Nada para atualizar";
} else echo "Anuncio pausado";
} else {
  echo "Anúncio do tipo Gratuito ou produto usado";
  $log = new WC_Logger();
  $log_entry .= __('Problema: O produto '. $productMlb . ' não foi atualizado por ser um Anúncio do tipo Gratuito ('.$productMlDetails->listing_type_id.') ou produto usado ('.$productMlDetails->condition.')', true );
  $log->add( 'wcmeli', $log_entry );
}
}
 ?>
