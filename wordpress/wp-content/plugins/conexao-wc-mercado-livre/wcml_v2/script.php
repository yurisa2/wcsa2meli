<?php
/**
 * @package Conexão Wc Mercado Livre
 * @author Luigi Muzy & Yuri Sá
 * @license
 * @copyright 2019 SAFRA Web
 */
 ini_set("error_reporting",E_ALL);
 ini_set('display_errors', 1);
 require_once __DIR__.'/../../../../wp-load.php';
$upload_dir = wp_upload_dir( null, false );

if(!file_exists($upload_dir['basedir'])) mkdir($upload_dir['basedir']);
if(!file_exists($upload_dir['basedir'] . '/wc-logs/')) mkdir($upload_dir['basedir'] . '/wc-logs/');


include WCMELI_PLUGIN_PATH.'variables.php';
include_once WCMELI_PLUGIN_PATH.'products.php';
include_once 'include/all_include.php';
/**
* Check if WooCommerce is active
**/

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && isset($objToken) ) {
$wcmeli_plugin_dir = plugin_dir_path(__FILE__);
$flux = new flux('wcmeli_product');
if(!file_exists($wcmeli_plugin_dir.'include/files/wcxml.mlb')) file_put_contents($wcmeli_plugin_dir.'include/files/wcxml.mlb',$flux->fluxCode(json_encode([])));
$wcxml = (array)json_decode($flux->fluxiCode(file_get_contents($wcmeli_plugin_dir.'include/files/wcxml.mlb')),true);

foreach ($wcxml as $key => $value) {
  $mlProductIdList[] = $value['mlb'];
}

if(!file_exists($wcmeli_plugin_dir.'include/files/ai_sk')) file_put_contents($wcmeli_plugin_dir.'include/files/ai_sk',json_encode(array('time'=>0)));

$content = (array)json_decode(file_get_contents($wcmeli_plugin_dir.'include/files/ai_sk'),true);
if($content['time'] + 1440 <= time()) {
  $getdata = urlencode(base64_encode(json_encode(array('f'=>get_site_url(),'t'=>time()))));
  $wp_remote_get_param = array(
    'timeout'     => 10
  );
  $response = json_decode(wp_remote_retrieve_body(wp_remote_get('https://sa2.com.br/wcsa2meli/wcmeli.php?gas='.$getdata,$wp_remote_get_param)));
  file_put_contents($wcmeli_plugin_dir.'include/files/ai_sk',json_encode(array('time'=>time(),'a'=>$response[0],'b'=>$response[1])));
  $content = (array)json_decode(file_get_contents($wcmeli_plugin_dir.'include/files/ai_sk'),true);
}

if(!defined('WCMELI_APP_ID')) define("WCMELI_APP_ID",base64_decode($content['a']));
if(!defined('WCMELI_SECRET_KEY')) define("WCMELI_SECRET_KEY",base64_decode($content['b']));

$wcmeliProduct = new meliProduct();

include 'includes/wcmeli_post.php';

  $fluxProduct = new flux('wcmeli_product');
  $fluxProduct->timeFile = true;
  $fluxProduct->nfeFile = false;
  $fluxProduct->storeOrderList = false;
  $fluxProduct->pathListItem = true;

  if(!$fluxProduct->setFiles()) {
    // $listProductId = $wcmeliProduct->meliGetProducts();
    $fluxProduct->list_item = $mlProductIdList;
    $fluxProduct->setFiles();
  }
  $fluxProduct->getFiles();
  $fluxProduct->addCounter();
  $productMlb = $fluxProduct->test_next_item();

  if(!$productMlb) {
    echo "Todos os itens foram usados. Recomeçando a partir da proxima execução";
  } else {

    foreach ($wcxml as $key => $value) {
      if($value['mlb'] == $productMlb) {
        if($value['wc_type'] == 'id') $wcProductId = $value['wc'];
        if($value['wc_type'] == 'sku') $wcProductSku = $value['wc'];
        break;
      }
    }

    if(!isset($wcProductId) && !isset($wcProductSku)) {
      echo "Produto $productMlb não foi encontrado no WooCommerce";
    } else {

      if(isset($wcProductSku)) {
        $wcProductId = wc_get_product_id_by_sku($wcProductSku);
      }
      if(!$wcProductId) {
        if(isset($wcProductSku)) {
          $wcProductId = wc_get_product_id_by_sku($wcProductSku);
        }
        if(!$wcProductId) {
          foreach ($wcxml as $key => $value) {
            if($value['mlb'] != $productMlb) {
              $syncProds[] = $value;
            }
          }
          var_dump(file_put_contents($wcmeli_plugin_dir.'include/files/wcxml.mlb',$flux->fluxCode(json_encode($syncProds))));
        }
        exit;
      }
      $productInfo = wc_get_product($wcProductId);

      echo '<pre>'.$wcProductId.' | '.$productMlb;

      if($productInfo->get_type() == 'grouped') {
        include $wcmeli_plugin_dir . 'includes/wcmeli_grouped_product.php';
      }
      if($productInfo->get_type() == 'variable') {
        include $wcmeli_plugin_dir . 'includes/wcmeli_variable_product.php';
      }
      if($productInfo->get_type() == 'external') {
        include $wcmeli_plugin_dir . 'includes/wcmeli_external_product.php';
      }
      if($productInfo->get_type() == 'simple') {
        include $wcmeli_plugin_dir . 'includes/wcmeli_simple_product.php';
      }
    }
  }
  $fluxProduct->add_item($productMlb);

  include_once('includes/wcmeli_order.php');

  include_once('includes/wcmeli_product_questions.php');

  include_once('includes/wcmeli_order_questions_sales.php');

  include_once('includes/wcmeli_product_images.php');
} else {
  $r = add_action( 'admin_notices', 'wcsa2meliDesabledPluginMessage' );
}

?>
