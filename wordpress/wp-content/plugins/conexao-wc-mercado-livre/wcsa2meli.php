<?php
/**
* @package Conexão Wc Mercado Livre
* @version 1.3.1
*/
/**
 * Plugin Name:       Conexão Wc Mercado Livre
 * Description:       Este plugin permite a sincronização automática dos produtos cadastrados no Mercado Livre com os produtos cadastrados no Woocommerce através do vínculo do SKU do produto.
 * Version:           1.3.2
 * Requires at least: 5.2
 * Requires PHP:      5.6
 * Author:            SAFRA - Web Solutions
 * Author URI:        https://www.facebook.com/safraweb/
 */
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

define('WCMELI_PLUGIN_PATH', plugin_dir_path(__FILE__));

include_once WCMELI_PLUGIN_PATH. "db.php";
include_once WCMELI_PLUGIN_PATH. "variables.php";
include_once WCMELI_PLUGIN_PATH. "template.php";
include_once WCMELI_PLUGIN_PATH. "cron.php";
include_once WCMELI_PLUGIN_PATH. "state_col.php";
include_once WCMELI_PLUGIN_PATH. "notices_function.php";
include_once WCMELI_PLUGIN_PATH. "notices.php";
include_once WCMELI_PLUGIN_PATH. "noideas.php";
include_once WCMELI_PLUGIN_PATH. "style.php";
?>
